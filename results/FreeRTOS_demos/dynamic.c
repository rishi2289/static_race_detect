/*
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat -I FreeRTOS/Demo/Common/include --domay_race dynamic.c 2> dynamic.txt
*/

#include <stdlib.h>

/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* Demo app include files. */
#include "dynamic.h"

/* Function that implements the "limited count" task as described above. */
static portTASK_FUNCTION_PROTO( FvLimitedIncrementTaskxLimitedIncrementHandleH, pvParameters );

/* Function that implements the "continuous count" task as described above. */
static portTASK_FUNCTION_PROTO( FvContinuousIncrementTaskxContinuousIncrementHandleH, pvParameters );

/* Function that implements the controller task as described above. */
static portTASK_FUNCTION_PROTO( FvCounterControlTaskH, pvParameters );

static portTASK_FUNCTION_PROTO( FvQueueReceiveWhenSuspendedTaskH, pvParameters );
static portTASK_FUNCTION_PROTO( FvQueueSendWhenSuspendedTaskH, pvParameters );

/* Demo task specific constants. */
#ifndef priSUSPENDED_RX_TASK_STACK_SIZE
	#define priSUSPENDED_RX_TASK_STACK_SIZE			( configMINIMAL_STACK_SIZE )
#endif
#define priSTACK_SIZE				( configMINIMAL_STACK_SIZE )
#define priSLEEP_TIME				pdMS_TO_TICKS( 128 )
#define priLOOPS					( 5 )
#define priMAX_COUNT				( ( uint32_t ) 0xff )
#define priNO_BLOCK					( ( TickType_t ) 0 )
#define priSUSPENDED_QUEUE_LENGTH	( 1 )

/*-----------------------------------------------------------*/

/* Handles to the two counter tasks.  These could be passed in as parameters
to the controller task to prevent them having to be file scope. */
static TaskHandle_t vContinuousIncrementTaskxContinuousIncrementHandleH, vLimitedIncrementTaskxLimitedIncrementHandleH;

/* The shared counter variable.  This is passed in as a parameter to the two
counter variables for demonstration purposes. */
static uint32_t ulCounter;

/* Variables used to check that the tasks are still operating without error.
Each complete iteration of the controller task increments this variable
provided no errors have been found.  The variable maintaining the same value
is therefore indication of an error. */
static volatile uint16_t usCheckVariable = ( uint16_t ) 0;
static volatile BaseType_t xSuspendedQueueSendError = pdFALSE;
static volatile BaseType_t xSuspendedQueueReceiveError = pdFALSE;

/* Queue used by the second test. */
QueueHandle_t xSuspendedTestQueue;

void lock(const char *s)
{
	;
}

void unlock(const char *s)
{
	;
}


/* The value the queue receive task expects to receive next.  This is file
scope so xAreDynamicPriorityTasksStillRunning() can ensure it is still
incrementing. */
static uint32_t ulExpectedValue = ( uint32_t ) 0;

/*-----------------------------------------------------------*/
/*
 * Start the three tasks as described at the top of the file.
 * Note that the limited count task is given a higher priority.
 */
void vStartDynamicPriorityTasks( void )
{
	xSuspendedTestQueue = xQueueCreate( priSUSPENDED_QUEUE_LENGTH, sizeof( uint32_t ) );

	if( xSuspendedTestQueue != NULL )
	{
		/* vQueueAddToRegistry() adds the queue to the queue registry, if one is
		in use.  The queue registry is provided as a means for kernel aware
		debuggers to locate queues and has no purpose if a kernel aware debugger
		is not being used.  The call to vQueueAddToRegistry() will be removed
		by the pre-processor if configQUEUE_REGISTRY_SIZE is not defined or is
		defined to be less than 1. */
		vQueueAddToRegistry( xSuspendedTestQueue, "Suspended_Test_Queue" );

		xTaskCreate( FvContinuousIncrementTaskxContinuousIncrementHandleH, "CNT_INC", priSTACK_SIZE, ( void * ) &ulCounter, tskIDLE_PRIORITY, NULL );
		FvContinuousIncrementTaskxContinuousIncrementHandleH(( void * ) &ulCounter);
		xTaskCreate( FvLimitedIncrementTaskxLimitedIncrementHandleH, "LIM_INC", priSTACK_SIZE, ( void * ) &ulCounter, tskIDLE_PRIORITY + 1, NULL );
		FvLimitedIncrementTaskxLimitedIncrementHandleH(( void * ) &ulCounter);
		xTaskCreate( FvCounterControlTaskH, "C_CTRL", priSUSPENDED_RX_TASK_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );
		xTaskCreate( FvQueueSendWhenSuspendedTaskH, "SUSP_TX", priSTACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );
		xTaskCreate( FvQueueReceiveWhenSuspendedTaskH, "SUSP_RX", priSUSPENDED_RX_TASK_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL );
	}
}
/*-----------------------------------------------------------*/

/*
 * Just loops around incrementing the shared variable until the limit has been
 * reached.  Once the limit has been reached it suspends itself.
 */
static portTASK_FUNCTION( FvLimitedIncrementTaskxLimitedIncrementHandleH, pvParameters )
{
	lock("P");

volatile uint32_t *pulCounter;

	/* Take a pointer to the shared variable from the parameters passed into
	the task. */
	pulCounter = ( volatile uint32_t * ) pvParameters;

	/* This will run before the control task, so the first thing it does is
	suspend - the control task will resume it when ready. */
	vTaskSuspend( NULL );

	for( ;; )
	{
		/* Just count up to a value then suspend. */
		( *pulCounter )++;

		if( *pulCounter >= priMAX_COUNT )
		{
			vTaskSuspend( NULL );
		}
	}
}
/*-----------------------------------------------------------*/

/*
 * Just keep counting the shared variable up.  The control task will suspend
 * this task when it wants.
 */
static portTASK_FUNCTION( FvContinuousIncrementTaskxContinuousIncrementHandleH, pvParameters )
{
	lock("P");
volatile uint32_t *pulCounter;
UBaseType_t uxOurPriority;

	/* Take a pointer to the shared variable from the parameters passed into
	the task. */
	pulCounter = ( volatile uint32_t * ) pvParameters;

	/* Query our priority so we can raise it when exclusive access to the
	shared variable is required. */
	uxOurPriority = uxTaskPriorityGet( NULL );

	for( ;; )
	{
		/* Raise the priority above the controller task to ensure a context
		switch does not occur while the variable is being accessed. */
		vTaskPrioritySet( NULL, tskIDLE_PRIORITY + 1 );
		{
			configASSERT( ( uxTaskPriorityGet( NULL ) == ( uxOurPriority + 1 ) ) );
			( *pulCounter )++;
		}
		vTaskPrioritySet( NULL, tskIDLE_PRIORITY );

		#if( configUSE_PREEMPTION == 0 )
			taskYIELD();
		#endif

		configASSERT( ( uxTaskPriorityGet( NULL ) == uxOurPriority ) );
	}
}
/*-----------------------------------------------------------*/

/*
 * Controller task as described above.
 */
static portTASK_FUNCTION( FvCounterControlTaskH, pvParameters )
{
	lock("P");
uint32_t ulLastCounter;
short sLoops;
short sError = pdFALSE;

	/* Just to stop warning messages. */
	( void ) pvParameters;

	for( ;; )
	{
		/* Start with the counter at zero. */
		ulCounter = ( uint32_t ) 0;

		/* First section : */

		/* Check the continuous count task is running. */
		for( sLoops = 0; sLoops < priLOOPS; sLoops++ )
		{
			/* Suspend the continuous count task so we can take a mirror of the
			shared variable without risk of corruption.  This is not really
			needed as the other task raises its priority above this task's
			priority. */
			vTaskSuspend( vContinuousIncrementTaskxContinuousIncrementHandleH );
			{
				lock("vContinuousIncrementTaskxContinuousIncrementHandleH");
				#if( INCLUDE_eTaskGetState == 1 )
				{
					configASSERT( eTaskGetState( vContinuousIncrementTaskxContinuousIncrementHandleH ) == eSuspended );
				}
				#endif /* INCLUDE_eTaskGetState */

				ulLastCounter = ulCounter;
			}
			unlock("vContinuousIncrementTaskxContinuousIncrementHandleH");
			vTaskResume( vContinuousIncrementTaskxContinuousIncrementHandleH );

			#if( configUSE_PREEMPTION == 0 )
				taskYIELD();
			#endif

			#if( INCLUDE_eTaskGetState == 1 )
			{
				configASSERT( eTaskGetState( vContinuousIncrementTaskxContinuousIncrementHandleH ) == eReady );
			}
			#endif /* INCLUDE_eTaskGetState */

			/* Now delay to ensure the other task has processor time. */
			vTaskDelay( priSLEEP_TIME );

			/* Check the shared variable again.  This time to ensure mutual
			exclusion the whole scheduler will be locked.  This is just for
			demo purposes! */
			vTaskSuspendAll();
			lock("A");
			{
				if( ulLastCounter == ulCounter )
				{
					/* The shared variable has not changed.  There is a problem
					with the continuous count task so flag an error. */
					sError = pdTRUE;
				}
			}
			unlock("A");
			xTaskResumeAll();
		}

		/* Second section: */

		/* Suspend the continuous counter task so it stops accessing the shared
		variable. */
		vTaskSuspend( vContinuousIncrementTaskxContinuousIncrementHandleH );
		lock("vContinuousIncrementTaskxContinuousIncrementHandleH");

		/* Reset the variable. */
		ulCounter = ( uint32_t ) 0;

		#if( INCLUDE_eTaskGetState == 1 )
		{
			configASSERT( eTaskGetState( vLimitedIncrementTaskxLimitedIncrementHandleH ) == eSuspended );
		}
		#endif /* INCLUDE_eTaskGetState */

		/* Resume the limited count task which has a higher priority than us.
		We should therefore not return from this call until the limited count
		task has suspended itself with a known value in the counter variable. */
		unlock("vLimitedIncrementTaskxLimitedIncrementHandleH");
		vTaskResume( vLimitedIncrementTaskxLimitedIncrementHandleH );

		#if( configUSE_PREEMPTION == 0 )
			taskYIELD();
		#endif

		/* This task should not run again until vLimitedIncrementTaskxLimitedIncrementHandleH has
		suspended itself. */
		#if( INCLUDE_eTaskGetState == 1 )
		{
			configASSERT( eTaskGetState( vLimitedIncrementTaskxLimitedIncrementHandleH ) == eSuspended );
		}
		#endif /* INCLUDE_eTaskGetState */

		/* Does the counter variable have the expected value? */
		if( ulCounter != priMAX_COUNT )
		{
			sError = pdTRUE;
		}

		if( sError == pdFALSE )
		{
			/* If no errors have occurred then increment the check variable. */
			portENTER_CRITICAL();
			lock("C");
				usCheckVariable++;
			unlock("C");
			portEXIT_CRITICAL();
		}

		/* Resume the continuous count task and do it all again. */
		unlock("vContinuousIncrementTaskxContinuousIncrementHandleH");
		vTaskResume( vContinuousIncrementTaskxContinuousIncrementHandleH );

		#if( configUSE_PREEMPTION == 0 )
			taskYIELD();
		#endif
	}
}
/*-----------------------------------------------------------*/

static portTASK_FUNCTION( FvQueueSendWhenSuspendedTaskH, pvParameters )
{
	lock("P");
static uint32_t ulValueToSend = ( uint32_t ) 0;

	/* Just to stop warning messages. */
	( void ) pvParameters;

	for( ;; )
	{
		vTaskSuspendAll();
		lock("A");
		{
			/* We must not block while the scheduler is suspended! */
			if( xQueueSend( xSuspendedTestQueue, ( void * ) &ulValueToSend, priNO_BLOCK ) != pdTRUE )
			{
				xSuspendedQueueSendError = pdTRUE;
			}
		}
		unlock("A");
		xTaskResumeAll();

		vTaskDelay( priSLEEP_TIME );

		++ulValueToSend;
	}
}
/*-----------------------------------------------------------*/

static portTASK_FUNCTION( FvQueueReceiveWhenSuspendedTaskH, pvParameters )
{
	lock("P");
uint32_t ulReceivedValue;
BaseType_t xGotValue;

	/* Just to stop warning messages. */
	( void ) pvParameters;

	for( ;; )
	{
		do
		{
			/* Suspending the scheduler here is fairly pointless and
			undesirable for a normal application.  It is done here purely
			to test the scheduler.  The inner xTaskResumeAll() should
			never return pdTRUE as the scheduler is still locked by the
			outer call. */
			vTaskSuspendAll();
			lock("A");
			{
				vTaskSuspendAll();
				lock("A");
				{
					xGotValue = xQueueReceive( xSuspendedTestQueue, ( void * ) &ulReceivedValue, priNO_BLOCK );
				}
				unlock("A");
				if( xTaskResumeAll() != pdFALSE )
				{
					xSuspendedQueueReceiveError = pdTRUE;
				}
			}
			unlock("A");
			xTaskResumeAll();

			#if configUSE_PREEMPTION == 0
			{
				taskYIELD();
			}
			#endif

		} while( xGotValue == pdFALSE );

		if( ulReceivedValue != ulExpectedValue )
		{
			xSuspendedQueueReceiveError = pdTRUE;
		}

		if( xSuspendedQueueReceiveError != pdTRUE )
		{
			/* Only increment the variable if an error has not occurred.  This
			allows xAreDynamicPriorityTasksStillRunning() to check for stalled
			tasks as well as explicit errors. */
			++ulExpectedValue;
		}
	}
}
/*-----------------------------------------------------------*/

/* Called to check that all the created tasks are still running without error. */
BaseType_t xAreDynamicPriorityTasksStillRunning( void )
{
/* Keep a history of the check variables so we know if it has been incremented
since the last call. */
static uint16_t usLastTaskCheck = ( uint16_t ) 0;
static uint32_t ulLastExpectedValue = ( uint32_t ) 0U;
BaseType_t xReturn = pdTRUE;

	/* Check the tasks are still running by ensuring the check variable
	is still incrementing. */

	if( usCheckVariable == usLastTaskCheck )
	{
		/* The check has not incremented so an error exists. */
		xReturn = pdFALSE;
	}

	if( ulExpectedValue == ulLastExpectedValue )
	{
		/* The value being received by the queue receive task has not
		incremented so an error exists. */
		xReturn = pdFALSE;
	}

	if( xSuspendedQueueSendError == pdTRUE )
	{
		xReturn = pdFALSE;
	}

	if( xSuspendedQueueReceiveError == pdTRUE )
	{
		xReturn = pdFALSE;
	}

	usLastTaskCheck = usCheckVariable;
	ulLastExpectedValue = ulExpectedValue;

	return xReturn;
}
