/*
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --inline=delay --inline=betoh --inline=atof --inline=BackendCons --inline=lightwarei2cCons --inline=update --inline=update_LGW_I2C --inline=update_LGW_SER --inline=distance_cm_orient --inline=distance_cm --inline=get_reading_SER --inline=read_rangefinder --inline=_fastloop_fn --inline=uart_begin --inline=get_reading --inline=_timer_tick rangefinder.c 2> inline.txt
cilly -c -w -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --flag="_lock_rx_in_timer_tick" --domay_race rangefinder.cil.c 2> rangefinder.txt
*/

/*
     C version of a part of ArduPilot code.
     This implements LightWare I2C and Serial Range Finder drivers.
     The main thread gets data which is fed by the I2C and UART bus threads.
*/

/* Standard includes */
#include <time.h>

/* Scheduler include files */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* ----------------- Helpers -----------------------*/

// high number indicates high priority
#define APM_MONITOR_PRIORITY 183
#define APM_MAIN_PRIORITY          180
#define APM_TIMER_PRIORITY         181
#define APM_RCIN_PRIORITY           177
#define APM_I2C_PRIORITY              176
#define APM_UART_PRIORITY            60
#define APM_STORAGE_PRIORITY    59
#define APM_IO_PRIORITY                  58
#define APM_STARTUP_PRIORITY     10
#define APM_SCRIPTING_PRIORITY    0

#define HAL_SEMAPHORE_BLOCK_FOREVER 0xffff

#define MINIMAL_STACK_SIZE   ( ( unsigned short ) 100 )

/* --------------- Helper functions ------------- */
/* delay */
void delay(int sec) {
   int milli = 1000 * sec;

   clock_t start_time = clock();

   while (clock() < start_time + milli)
      ;
}

int betoh(int val) {
    return val;
}

int atof(int a) {
   return a+1; //some op not of relevance
}


/* -------------- Global variables: Task Handles ------------- */
TaskHandle_t* mainmainTskH;
TaskHandle_t* uart_threaduart_thread_ctxH;
TaskHandle_t* _timer_thread_ctx;
TaskHandle_t* timerthread_ctxH;      //I2C Bus thread
TaskHandle_t* rxbuff_full_irqrxdmaH;  // DMA Read ISR
TaskHandle_t* txdma;  //DMA Write ISR

/* ---------- Global variables: Semaphores --------------- */
SemaphoreHandle_t semaphore = NULL;    //I2CDevice Bus semaphore
SemaphoreHandle_t _timer_semaphore = NULL;
SemaphoreHandle_t bsemaphore = NULL;

/*----------------- Global variables: Queues -------------------*/
QueueHandle_t EventQueue;

#define block_one_tick					( ( TickType_t ) 1 )

void lock(const char *s)
{
  ;
}

void unlock(const char *s)
{
  ;
}


/* class RangerFinder defined in RangerFinder.h */

enum Rotation {
    ROTATION_NONE                = 0,
    ROTATION_YAW_45              = 1,
    ROTATION_YAW_90              = 2,
    ROTATION_ROLL_180            = 8,
    ROTATION_ROLL_180_YAW_45     = 9,
    ROTATION_PITCH_90            = 24,
    ROTATION_PITCH_270           = 25,
};

struct RangeFinder_State{
     int distance_cm;
};

/* ------------ Global variables: shared ---------------- */
struct RangeFinder_State state;
int _lock_rx_in_timer_tick;  // a flag used in _timer_tick
int _in_timer;  // a flag
int _readbuf;  //  ring buffer, here abstracted to int variable
int _writebuf;  //  ring buffer, here abstracted to int variable
int rx_bounce_buf;  // bounce buffer, an int pointer here abstracted to int variable
int thread_started = 0;

/* ---------------- Function declarations -------------- */

void BackendCons(struct RangeFinder_State _state);  //Runs in main thread
void lightwarei2cCons(struct RangeFinder_State _state);  //Runs in main thread
void update();  //Runs in main thread
void update_LGW_I2C();  //Runs in main thread
void update_LGW_SER();  //Runs in main thread
int distance_cm_orient(enum Rotation orientation);  //Runs in main thread
int distance_cm();  //Runs in main thread
int get_reading_SER(); //Runs in main thread
void read_rangefinder();  //Runs in main thread
void _fastloop_fn();  //Runs in main thread
void uart_begin(); //Runs in main thread

int get_reading();  //Runs in I2C Bus Thread
void _timer_tick();  //Runs in UART thread
static portTASK_FUNCTION( Ftimerthread_ctxH, pvParameters );      // Runs in I2C Bus Thread

static portTASK_FUNCTION( Fuart_threaduart_thread_ctxH, pvParameters ); //Runs in UART thread

static portTASK_FUNCTION( Frxbuff_full_irqrxdmaH, pvParameters ); //DMA ISR
static portTASK_FUNCTION( FmainmainTskH, pvParameters );   //main() runs as main thread

/* ----------------- Function definitions -------------- */

void update() {             /* RangeFinder.cpp */
     update_LGW_I2C(); 
     update_LGW_SER();
}

int distance_cm_orient(enum Rotation orientation) {   /* RangeFinder.cpp */  /* runs in main thread */
    return distance_cm();
}

int distance_cm() {     /* RangeFinder_Backend.h */  /* runs in main thread */
    return state.distance_cm; 
}

/* class AP_RangeFinder_Backend defined in RangeFinder_Backend.h */

void BackendCons(struct RangeFinder_State _state) { //Runs in main thread
     state = _state;
}

/* class AP_RangeFinder_LightwareI2C defined in AP_RangeFinder_LightwareI2C.h */

void lightwarei2cCons(struct RangeFinder_State _state) { //Runs in main thread
    BackendCons(_state);
}

void update_LGW_I2C() { //Runs in main thread
    // nothing to do - its all done in the timer()
}

void update_LGW_SER() {  //Runs in main thread
    int x;

    if (get_reading_SER()) {
            if (state.distance_cm > 50) {
                x++;
            } else if (state.distance_cm < 10) {
                x--;
            } else {
                x = x+2;
            }
    } else if (x > 200) {
        x=0;
    }
}

static portTASK_FUNCTION( Ftimerthread_ctxH, pvParameters ) {   /* This runs in I2CDevice bus thread */
     lock("P");
     int x;  //variable not of relevance

     while(1){
             if (xSemaphoreTake(semaphore, HAL_SEMAPHORE_BLOCK_FOREVER)) {
                 lock("semaphore");
                 if (get_reading()){
                      if (state.distance_cm > 50) {
                         x++;    //some op not of relevance
                      } else if (state.distance_cm < 10) {
                         x--;  // some op not of relevance
                      } else {
                         x=x+2;  //some op not of relevance
                      }
                 }
                 unlock("semaphore");
                 xSemaphoreGive(semaphore);
           }

        delay(5);
    } // end while
}

int get_reading() {     /* This runs in I2CDevice bus thread */

     //get data from sensor
     int ret;
     int val;

//     ret = read((uint8_t *) &val, sizeof(val));		//TODO: need to INLINE??
     if (ret)
          state.distance_cm = betoh(val);
     return ret;
}

int get_reading_SER() {     /* This runs in main thread */

     //get data from sensor
     int ret;
     int sum;
     int nbytes = 10;   //some init value
     int linebuf;  // an array of char, here abstracted to int variable


    while (nbytes-- > 0){
    	char c;
       // char c = uart->read();   //TODO: INLINE??  //get data from UART bus thread
        if (c == '\r'){
            linebuf = 10;
            sum+=atof(linebuf);
        }
        else{
            linebuf = 1;
        }
    }//end while nbytes 

   state.distance_cm = 100*sum;
   return 1;
}

/* classs Copter defined in Copter.h */

void read_rangefinder() {        /* sensors.cpp */

    update();
    int temp_alt = distance_cm_orient(ROTATION_PITCH_270); 

    // wp_nav->set_rangefinder_alt(rangefinder_state.enabled, rangefinder_state.alt_healthy, rangefinder_state.alt_cm_filt.get());
}

void _fastloop_fn() {  //This runs in main thread
  delay(1);   //wait for sample
  return;   //do nothing, for now.
}

void uart_begin() {  //This runs in main thread
    int x;   //some variable
    int _device_initialised; 

    xTaskCreate(Fuart_threaduart_thread_ctxH, "apm_uart", MINIMAL_STACK_SIZE, NULL, APM_UART_PRIORITY, NULL);
    // Fuart_threaduart_thread_ctxH();

    //allocate the read buffer
    while (_in_timer) { 
        delay(1);
    }

    if (x == 20) {
        _readbuf = 0;  //conditionally set the size of _readbuf
    }

   if (x == 10) {
       _readbuf = 10; //conditionally clear the _readbuf content
   }

    //allocate the write buffer
    while (_in_timer) { 
        delay(1);
    }

    if (x == 21) {
        _writebuf = 0;  //conditionally set the size of _writebuf
    }

   if (x == 10) {
       _writebuf = 10; //conditionally clear the _writebuf content
   }

  if (x != 0) {  // some condition
     if (!_device_initialised) {  // some condition
         if (x > 2) {  // some condition
              // chSysLock();  //CHECK: use disable interrupt FreeRTOS API
			portENTER_CRITICAL();
			lock("C");
              //allocate DMA stream
              xTaskCreate(Frxbuff_full_irqrxdmaH, "DMA_R_IRQ", MINIMAL_STACK_SIZE, NULL, 192, NULL); //This is an DMA ISR
              // Frxbuff_full_irqrxdmaH();
			unlock("C");
			portEXIT_CRITICAL();
              // chSysUnlock(); //CHECK: use enable isable interrupt FreeRTOS API
         }

        _device_initialised = 1;
     } //(!_device_initialised)

     //TODO: assoicate the memory destination rx_bounce_buf to the DMA stream
     //enable DMA. // The rxbuf_full_irq ISR can be triggered from now.
  }//(x != 0)

}

static portTASK_FUNCTION( Fuart_threaduart_thread_ctxH, pvParameters ) {    //Runs in UART thread
  lock("P");
	BaseType_t mask;
	uint32_t ulReceivedValue;
    while(1){
        //mask = event wait for all events with timeout of 1   CHECK: equivalent FreeRTOS API
        mask = xQueueReceive( EventQueue, ( void * ) &ulReceivedValue, block_one_tick );
       if (mask) {
           _timer_tick();
       }
    }//while(1)
}

void _timer_tick() {  // Runs in UART thread
    int x;  //some variable, of no relevance
    if (x == 10) {  //some condition

        _lock_rx_in_timer_tick = 1;  //set a flag
        lock("set__lock_rx_in_timer_tick");

        if ( x == 20) { //some condition
            if (x == 30) { //some condition
               //try to get data directly
                rx_bounce_buf = 10;  //cache invalidate
                _readbuf = rx_bounce_buf;  // copy data from rx_bounce_buf to _readbuf
            } //end of (x == 30)
           //restart DMA transfers
       }//(x == 20)

        unlock("set__lock_rx_in_timer_tick");
       _lock_rx_in_timer_tick = 0;  //reset the flag

   } // end of (x == 10)
}

static portTASK_FUNCTION( Frxbuff_full_irqrxdmaH, pvParameters ) {    /* This is DMA Transaction Complete ISR. */
lock("P");
lock("I");
 
    if (_lock_rx_in_timer_tick) { /* check a flag. _timer_tick is in progress */
        return;
    }

    lock("check__lock_rx_in_timer_tick");
    rx_bounce_buf = 10;
    int len = 10; 
    if (len > 0) {  /* if there is data to be read */
        rx_bounce_buf = 0;  //cache invalidate
        _readbuf = rx_bounce_buf;  // Copy data from rx_bounce_buf to _readbuf
    }
    
    //restart the DMA transfers
   /* Associate the memory destination rx_bounce_buf to the DMA stream stm32_dma_stream_t rxdma. */
   /* DMA stream enable. */ // The rxbuf_full_irq ISR can be triggered from now.
}

static portTASK_FUNCTION( FmainmainTskH, pvParameters ) {   //main() runs as main thread
	lock("P");
     
     // vTaskPrioritySet(mainmainTskH, APM_MAIN_PRIORITY);  //CHECK: data type of priority

     uart_begin();
    
     vTaskPrioritySet(mainmainTskH, APM_STARTUP_PRIORITY); //CHECK: data type of priority

    //init_rangefinder(); expanded.
    struct RangeFinder_State _state;   //some variable, of no relevance
    lightwarei2cCons(_state);
    vSemaphoreCreateBinary(semaphore); //CHECK: later version supports xSemaphoreCreateBinary()
    xTaskCreate(Ftimerthread_ctxH, "I2C", MINIMAL_STACK_SIZE, NULL, APM_I2C_PRIORITY, NULL); //made timer() run instead of bus_thread()
    // Ftimerthread_ctxH();

    vTaskPrioritySet(mainmainTskH, APM_MAIN_PRIORITY);  //CHECK: data type of priority

     while(1){
          _fastloop_fn();
 
           int time_available = 10;
           if (time_available){
                read_rangefinder();
          }
     }

    // return 0; 
} 

int main() {
     
    xTaskCreate(FmainmainTskH, "main", MINIMAL_STACK_SIZE, NULL, APM_MAIN_PRIORITY, NULL); //made timer() run instead of bus_thread()
    // FmainmainTskH();
    return 0; 
} 