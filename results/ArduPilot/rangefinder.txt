SHARED ACCESSES:


1 rangefinder.c:214 timerthread_ctxH state.distance_cm W (176,176) semaphore P
2 rangefinder.c:190 timerthread_ctxH state.distance_cm R (176,176) semaphore P
3 rangefinder.c:192 timerthread_ctxH state.distance_cm R (176,176) semaphore P
4 rangefinder.c:337 uart_threaduart_thread_ctxH rx_bounce_buf W (60,60) set__lock_rx_in_timer_tick P
5 rangefinder.c:338 uart_threaduart_thread_ctxH _readbuf W (60,60) set__lock_rx_in_timer_tick P
6 rangefinder.c:338 uart_threaduart_thread_ctxH rx_bounce_buf R (60,60) set__lock_rx_in_timer_tick P
7 rangefinder.c:358 rxbuff_full_irqrxdmaH rx_bounce_buf W (192,192) check__lock_rx_in_timer_tick I P
8 rangefinder.c:361 rxbuff_full_irqrxdmaH rx_bounce_buf W (192,192) check__lock_rx_in_timer_tick I P
9 rangefinder.c:362 rxbuff_full_irqrxdmaH _readbuf W (192,192) check__lock_rx_in_timer_tick I P
10 rangefinder.c:362 rxbuff_full_irqrxdmaH rx_bounce_buf R (192,192) check__lock_rx_in_timer_tick I P
11 rangefinder.c:271 mainmainTskH _readbuf W (180,180) P
12 rangefinder.c:275 mainmainTskH _readbuf W (180,180) P
13 rangefinder.c:284 mainmainTskH _writebuf W (180,180) P
14 rangefinder.c:288 mainmainTskH _writebuf W (180,180) P
15 rangefinder.c:153 mainmainTskH state W (10,10) P
16 rangefinder.c:239 mainmainTskH state.distance_cm W (180,180) P
17 rangefinder.c:170 mainmainTskH state.distance_cm R (180,180) P
18 rangefinder.c:172 mainmainTskH state.distance_cm R (180,180) P
19 rangefinder.c:147 mainmainTskH state.distance_cm R (180,180) P


CONFLICTS:


1 conflicts 16
POTENTIAL DATA-RACE

1 conflicts 17
POTENTIAL DATA-RACE

1 conflicts 18
POTENTIAL DATA-RACE

1 conflicts 19
POTENTIAL DATA-RACE

2 conflicts 16
POTENTIAL DATA-RACE

3 conflicts 16
POTENTIAL DATA-RACE

4 conflicts 7
DATA-RACE FREE

4 conflicts 8
DATA-RACE FREE

4 conflicts 10
DATA-RACE FREE

5 conflicts 9
DATA-RACE FREE

5 conflicts 11
POTENTIAL DATA-RACE

5 conflicts 12
POTENTIAL DATA-RACE

6 conflicts 7
DATA-RACE FREE

6 conflicts 8
DATA-RACE FREE

9 conflicts 11
POTENTIAL DATA-RACE

9 conflicts 12
POTENTIAL DATA-RACE



Total Conflicts: 16
Potential Data-Races: 10
Race-Free Pairs: 6

Total Time: 0.224956
