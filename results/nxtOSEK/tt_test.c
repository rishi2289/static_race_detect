/*
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --im_ceil=y --eq_fcfs=y --domay_race tt_test.c 2> tt_test.txt
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"


static portTASK_FUNCTION_PROTO( FLowTaskH, pvParameters );
static portTASK_FUNCTION_PROTO( FHighTaskH, pvParameters );
SemaphoreHandle_t mx;
#define priSTACK_SIZE       ( configMINIMAL_STACK_SIZE )


int digits;
int lowtaskcount;
int hightaskcount;


void lock(const char *s)
{
	;
}

void unlock(const char *s)
{
	;
}

void vtttest( void )
{
	mx = xSemaphoreCreateMutex();
	xTaskCreate( FLowTaskH, "LowTask", priSTACK_SIZE, NULL, 1, NULL );
	xTaskCreate( FHighTaskH, "HighTask", priSTACK_SIZE, NULL, 2, NULL );
	
}
/*-----------------------------------------------------------*/

static portTASK_FUNCTION(FLowTaskH, pvParameters )
{
  lock("P");
    int hcount, lcount;
    while(1)
  {

    for (hcount = 0; hcount < 10; hcount++) {
      digits--;
      if( xSemaphoreTake( mx, portMAX_DELAY ) == pdPASS )
      {
        //GetResource(mx);

      ecrobot_debug1(digits, lowtaskcount, hightaskcount);
        /* The following loop is to wait for the ISR triggered by ecrobot_debug
         * (or the specific I/O statement inside ecrobot_debug) to complete
         * This is a good example to show how explicit/implicit shared resource
         * is needed to be protected against concurrent accesses from multiple Tasks
       */
      for (lcount = 0; lcount < 3200; lcount++) ;

        xSemaphoreGive( mx);
        //ReleaseResource(mx);
      }
    }
    lowtaskcount++;
  }
  vTaskDelete(NULL);
}


static portTASK_FUNCTION(FHighTaskH, pvParameters )
{
  lock("P");
  int hcount, lcount;

  while(1) {

    for (hcount = 0; hcount < 10; hcount++) {
      if( xSemaphoreTake( mx, portMAX_DELAY ) == pdPASS )
      {
        //GetResource(mx);

      ecrobot_debug2(digits, hcount, 0);
          /* The following loop is to wait for the ISR triggered by ecrobot_debug
           * (or the specific I/O statement inside ecrobot_debug) to complete
           * This is a good example to show how explicit/implicit shared resource
           * is needed to be protected against concurrent accesses from multiple Tasks
         */
      for (lcount = 0; lcount < 3200; lcount++) ;

      xSemaphoreGive( mx);
      //ReleaseResource(mx);
      }
    }
    vTaskDelay(2000);
    //WaitEvent(BarrierEvent);
    //ClearEvent(BarrierEvent);
    digits = 0;
    hightaskcount++;

  }
  vTaskDelete(NULL);
}
