/*
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --inline=showInitScreen --inline=display_usb_data usb_test.c 2> inline.txt
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --im_ceil=y --eq_fcfs=y --domay_race usb_test.cil.c 2> usb_test.txt
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

//static TaskHandle_t HighTaskH;
typedef unsigned char U8;


static portTASK_FUNCTION_PROTO( FTask_ts1H, pvParameters );
static portTASK_FUNCTION_PROTO( FTask_backgroundH, pvParameters );
SemaphoreHandle_t USB_Rx;
#define priSTACK_SIZE       ( configMINIMAL_STACK_SIZE )
#define MAX_USB_DATA_LEN 64
#define DISCONNECT_REQ 0xFF
#define MAX_NUM_OF_CHAR 16
#define MAX_NUM_OF_LINE 8

static int pos_x = 0;
static int pos_y = 0;

static void showInitScreen(void)
{
  pos_x = 0;
  pos_y = 0;

  display_clear(0);
  display_goto_xy(0, 0);
  display_string("USB TEST");
  display_goto_xy(0, 1);
  display_string("Run usbhost.exe");
  display_update(); 
}

static void display_usb_data(U8 *data, int len)
{
  int i;

  /* set LCD postion in x, y */
  if (pos_x >= MAX_NUM_OF_CHAR)
  {
    pos_x = 0;
    pos_y++;
  }

  if (pos_y >= MAX_NUM_OF_LINE)
  {
    pos_x = 0;
    pos_y = 0;
  }
      
  if (pos_x == 0 && pos_y == 0)
  {
    display_clear(0);
  }
  display_goto_xy(pos_x, pos_y);

  for (i = 0; i < len; i++)
  {
    if (data[i] == '\n')
    {
      pos_x = 0;
      pos_y++;
      break;
    }
    else
    {
      display_string((char *)&data[i]);
      display_update();
      if (i == (len - 1))
      {
        pos_x += len;
        break;
      }
    }
  }
}

/* ECRobot hooks */
void ecrobot_device_initialize()
{
  ecrobot_init_usb(); /* init USB */
}

void ecrobot_device_terminate()
{
  ecrobot_term_usb(); /* terminate USB */
}

void lock(const char *s)
{
	;
}

void unlock(const char *s)
{
	;
}

void vusbtest( void )
{
	USB_Rx = xSemaphoreCreateMutex();
	xTaskCreate( FTask_ts1H, "LowTask", priSTACK_SIZE, NULL, 2, NULL );
	xTaskCreate( FTask_backgroundH, "HighTask", priSTACK_SIZE, NULL, 1, NULL );
	
}
/*-----------------------------------------------------------*/

static portTASK_FUNCTION(FTask_ts1H, pvParameters )
{
  lock("P");

  while(1)
  {
    vTaskDelay(1);
    if( xSemaphoreTake( USB_Rx, portMAX_DELAY ) == pdPASS )
    {
      //GetResource(USB_Rx);
      ecrobot_process1ms_usb(); /* USB process handler (must be invoked every 1msec) */
      xSemaphoreGive( USB_Rx);
      //ReleaseResource(USB_Rx);
    }
  }
  vTaskDelete(NULL);
  //TerminateTask();
}


static portTASK_FUNCTION(FTask_backgroundH, pvParameters )
{
  lock("P");
  int len;
  U8 data[MAX_USB_DATA_LEN]; /* first byte is preserved for disconnect request from host */

  showInitScreen();

  while(1) {
    memset(data, 0, MAX_USB_DATA_LEN); /* flush buffer */
    /* critical section */
    if( xSemaphoreTake( USB_Rx, portMAX_DELAY ) == pdPASS )
    {
      //GetResource(USB_Rx);
      len = ecrobot_read_usb(data, 0, MAX_USB_DATA_LEN); /* read USB data */
      xSemaphoreGive( USB_Rx);
      //ReleaseResource(USB_Rx);
    }

    if (len > 0)
    {
      if (data[0] == DISCONNECT_REQ)
      {
        /* disconnect current connection */
        ecrobot_disconnect_usb();
        showInitScreen();
      }
      else
      {
        data[0] = 0x00;
        ecrobot_send_usb(data, 0, len); /* send back received USB data */
        display_usb_data(&data[1], len-1);
      }
    }
  }
}
