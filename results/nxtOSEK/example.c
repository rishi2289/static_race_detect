/*
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --im_ceil=y --eq_fcfs=y --domay_race example.c 2> example.txt
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"


static portTASK_FUNCTION_PROTO( FT1H, pvParameters );
static portTASK_FUNCTION_PROTO( FI2H, pvParameters );
static portTASK_FUNCTION_PROTO( FI3H, pvParameters );
SemaphoreHandle_t r2,r3;
#define priSTACK_SIZE       ( configMINIMAL_STACK_SIZE )


int x,y,z;

void lock(const char *s)
{
	;
}

void unlock(const char *s)
{
	;
}

void vexample( void )
{
	r2 = xSemaphoreCreateMutex();
  r3 = xSemaphoreCreateMutex();
	xTaskCreate( FT1H, "T1", priSTACK_SIZE, NULL, 1, NULL );
  xTaskCreate( FI2H, "I2", priSTACK_SIZE, NULL, 2, NULL );
	xTaskCreate( FI3H, "I3", priSTACK_SIZE, NULL, 3, NULL );
	
}
/*-----------------------------------------------------------*/

static portTASK_FUNCTION(FT1H, pvParameters )
{
  lock("P");
  int t;
  if( xSemaphoreTake( r3, portMAX_DELAY ) == pdPASS )
  { //GetResource(r3);
    x=10;
    y=0;
    xSemaphoreGive(r3); //ReleaseResource(r3);
  }
  if( xSemaphoreTake( r2, portMAX_DELAY ) == pdPASS )
  { //GetResource(r2);
    t=x+y;
    x=t-x;
    xSemaphoreGive(r2); //ReleaseResource(r2);
  }
  z=t*2;
  if( xSemaphoreTake( r2, portMAX_DELAY ) == pdPASS )
  { //GetResource(r2);
    y=t-y;
    xSemaphoreGive(r2); //ReleaseResource(r2);
  }
}

static portTASK_FUNCTION(FI2H, pvParameters )
{
  lock("P");
  lock("I");
  if( xSemaphoreTake( r2, portMAX_DELAY ) == pdPASS )
  { //GetResource(r2);
    y++;
    x--;
    xSemaphoreGive(r2); //ReleaseResource(r2);
  }
}

static portTASK_FUNCTION(FI3H, pvParameters )
{
  lock("P");
  lock("I");
  if( xSemaphoreTake( r3, portMAX_DELAY ) == pdPASS )
  { //GetResource(r3);
    z=20;
    xSemaphoreGive(r3); //ReleaseResource(r3);
  }
}
