/*
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --im_ceil=y --eq_fcfs=y --domay_race res_test.c 2> res_test.txt
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

static TaskHandle_t HighTaskH;

static portTASK_FUNCTION_PROTO( FLowTaskH, pvParameters );
static portTASK_FUNCTION_PROTO( FHighTaskH, pvParameters );
SemaphoreHandle_t resource1;
#define priSTACK_SIZE       ( configMINIMAL_STACK_SIZE )

#define COUNT 5000000
int digits;
int lowtaskcount;
int hightaskcount;


void lock(const char *s)
{
	;
}

void unlock(const char *s)
{
	;
}

void vrestest( void )
{
	resource1 = xSemaphoreCreateMutex();
	xTaskCreate( FLowTaskH, "LowTask", priSTACK_SIZE, NULL, 2, NULL );
	xTaskCreate( FHighTaskH, "HighTask", priSTACK_SIZE, NULL, 3, NULL );
	
}
/*-----------------------------------------------------------*/

static portTASK_FUNCTION(FLowTaskH, pvParameters )
{
  lock("P");
  vTaskDelay(3000);

  while(1)
  {
    int rcount;

    ecrobot_debug1(digits, 1111, 0);
    for (rcount = 0; rcount < COUNT; rcount++);
    ecrobot_debug1(digits, 1119, 0);

    if( xSemaphoreTake( resource1, portMAX_DELAY ) == pdPASS )
    {
      //GetResource(resource1);

      for(rcount = 0; rcount < COUNT; rcount++) digits++;
      vTaskResume(HighTaskH);
      //SetEvent(HighTask, event1);
      ecrobot_debug1(digits, 1199, 0);
      for(rcount=0; rcount < COUNT; rcount++) digits--;
      
      xSemaphoreGive( resource1);
      //ReleaseResource(resource1);
    }
    ecrobot_debug1(digits, 1999, 0);

    vTaskDelay(10000);
  }
  vTaskDelete(NULL);
  //TerminateTask();
}


static portTASK_FUNCTION(FHighTaskH, pvParameters )
{
  lock("P");
  vTaskDelay(3000);
  while(1) {
    int rcount;

    ecrobot_debug2(digits, 2222, 1);
    for(rcount=0; rcount < COUNT; rcount++) digits++;
    ecrobot_debug2(digits, 2228, 1);
    for(rcount=0; rcount < COUNT; rcount++) digits++;
    
    vTaskSuspend(NULL);
    //WaitEvent(event1);
    //ClearEvent(event1);
    ecrobot_debug2(digits, 2288, 1);
    for(rcount=0; rcount < COUNT; rcount++) digits++;

    if( xSemaphoreTake( resource1, portMAX_DELAY ) == pdPASS )
    {
      //GetResource(resource1);
      ecrobot_debug2(digits, 2888, 1);
      for (rcount = 0; rcount < COUNT; rcount++);
      xSemaphoreGive( resource1);
      //ReleaseResource(resource1);
    }

    ecrobot_debug2(digits,8888 , 1);
    vTaskDelay(10000);
  }
  
  vTaskDelete(NULL);
}
