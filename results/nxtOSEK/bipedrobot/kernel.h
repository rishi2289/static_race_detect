#ifndef NXTOSEKKERNEL
#define NXTOSEKKERNEL
/*
---------------------------------------------------------------
   header generated from modularEmbeddedDSL via MPS
   resource NxtOsekKernel
---------------------------------------------------------------
*/

// custom includes for header
typedef int StatusType;

void TerminateTask(void);

void ShutdownOS(StatusType status);
#endif
