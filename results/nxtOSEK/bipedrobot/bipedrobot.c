/*
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --inline=setServo --inline=setMotion --inline=getMotionCommand --inline=sleep --inline=check_sleepers bipedrobot.c 2> inline.txt
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --im_ceil=y --eq_fcfs=y --domay_race bipedrobot.cil.c 2> bipedrobot.txt
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
//#include <unistd.h>

/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#include "ecrobot_interface.h"
#include "sleep.h"
#include "motion.h"


static portTASK_FUNCTION_PROTO( FTask_InitH, pvParameters );
static portTASK_FUNCTION_PROTO( FTask_CommanderH, pvParameters );
static portTASK_FUNCTION_PROTO( FTask_DisplayH, pvParameters );
static portTASK_FUNCTION_PROTO( FTask_MotionControlH, pvParameters );
SemaphoreHandle_t ResourceCommand;
static TaskHandle_t Task_MotionControlH;
#define priSTACK_SIZE       ( configMINIMAL_STACK_SIZE )
#define N_NXTe_CH   1
#define NXTe_PORT  NXT_PORT_S3

  

//#define NXTe_PORT 

/* robot motion command */
typedef int motionCmd_t;
static motionCmd_t motionCmd;
typedef unsigned char U8;
typedef signed char S8;


void lock(const char *s)
{
	;
}

void unlock(const char *s)
{
	;
}

typedef signed short    s16;
typedef s16 tpl_task_id;
typedef tpl_task_id     TaskType;

TaskType sleeperTaskID[N_SLEEPERS];
U32 sleepTaskCounter[N_SLEEPERS];
U8 isTaskSleeping[N_SLEEPERS] = {0};



const motion_t PRM_NEUTRAL[] = {
   {{{ 1000,  1000,  1000,  1000,  1000,  1000,  1000,  1000,  1000,  1000}},   0}
};

/* motion parameters for standing up */
const motion_t PRM_STAND_UP[] = {
   {{{ 1000,  1000,   800,  1200,   800,  1200,   800,  1200,  1000,  1000}}, 500}
};

/* motion parameters for going forward */
const motion_t PRM_FORWARD[] = {
   {{{  950,   950, BLANK, BLANK, BLANK, BLANK,   850, BLANK, BLANK,  1050}}, 200},
   {{{BLANK, BLANK,   300, BLANK,   400, BLANK,   900, BLANK,  1050,  1100}}, 100},
   {{{BLANK, BLANK,   200,  1100, BLANK, BLANK, BLANK,  1350,   850,   900}}, 200},
   {{{BLANK, BLANK,   800, BLANK,   800, BLANK,   850, BLANK, BLANK, BLANK}},   0},
   {{{BLANK, BLANK, BLANK,  1600, BLANK,  1500, BLANK, BLANK, BLANK, BLANK}}, 200},
   {{{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,  1200, BLANK, BLANK}},   0},
   {{{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,   700, BLANK,  1100,  1150}}, 100},
   {{{BLANK, BLANK, BLANK,  1200, BLANK,  1200, BLANK, BLANK,  1000,  1050}}, 200},
   {{{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,   850, BLANK, BLANK, BLANK}}, 100}
};

/* motion parameters for turning right */
const motion_t PRM_RIGHT_TURN[] = {
   {{{ 1050,  1050, BLANK, BLANK, BLANK, BLANK,   850, BLANK, BLANK,  1050}}, 200},
   {{{BLANK, BLANK,   300, BLANK,   400, BLANK,   900, BLANK,  1050,  1100}}, 100},
   {{{BLANK, BLANK,   200,  1100, BLANK, BLANK, BLANK,  1350,   850,   900}}, 200},
   {{{BLANK, BLANK,   800, BLANK,   800, BLANK,   850, BLANK, BLANK, BLANK}},   0},
   {{{BLANK, BLANK, BLANK,  1600, BLANK,  1500, BLANK, BLANK, BLANK, BLANK}}, 200},
   {{{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,  1200, BLANK, BLANK}},   0},
   {{{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,   700, BLANK,  1100,  1150}}, 100},
   {{{BLANK, BLANK, BLANK,  1200, BLANK,  1200, BLANK, BLANK,  1000,  1050}}, 200},
   {{{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,   850, BLANK, BLANK, BLANK}}, 100}
};

/* motion parameters for turning left */
const motion_t PRM_LEFT_TURN[] = {
   {{{  950,   950, BLANK, BLANK, BLANK, BLANK, BLANK,  1150,   950, BLANK}}, 200},
   {{{BLANK, BLANK, BLANK,  1700, BLANK,  1600, BLANK,  1100,   900,   950}}, 100},
   {{{BLANK, BLANK,   900,  1800, BLANK, BLANK,   650, BLANK,  1100,  1150}}, 200},
   {{{BLANK, BLANK, BLANK,  1200, BLANK,  1200, BLANK,  1150, BLANK, BLANK}},   0},
   {{{BLANK, BLANK,   400, BLANK,   500, BLANK, BLANK, BLANK, BLANK, BLANK}}, 200},
   {{{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,   800, BLANK, BLANK, BLANK}},   0},
   {{{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,  1300,   850,   900}}, 100},
   {{{BLANK, BLANK,   800, BLANK,   800, BLANK, BLANK, BLANK,   950,  1000}}, 200},
   {{{BLANK, BLANK, BLANK, BLANK, BLANK, BLANK, BLANK,  1150, BLANK, BLANK}}, 100}
};

/* robot motion paramters table */
const motionTable_t motionTable[] = {
  {(motion_t *)PRM_NEUTRAL,    sizeof(PRM_NEUTRAL)/sizeof(motion_t)},
  {(motion_t *)PRM_STAND_UP,   sizeof(PRM_STAND_UP)/sizeof(motion_t)},
  {(motion_t *)PRM_FORWARD,    sizeof(PRM_FORWARD)/sizeof(motion_t)},
  {(motion_t *)PRM_RIGHT_TURN, sizeof(PRM_RIGHT_TURN)/sizeof(motion_t)},
  {(motion_t *)PRM_LEFT_TURN,  sizeof(PRM_LEFT_TURN)/sizeof(motion_t)}
};




void ecrobot_device_initialize()
{
  ecrobot_init_i2c(NXTe_PORT, LOWSPEED_9V);
  ecrobot_init_bt_slave("nxtOSEK");
}

void ecrobot_device_terminate()
{
  ecrobot_term_i2c(NXTe_PORT);
  ecrobot_term_bt_connection();
}

void user_1ms_isr_type2(void)
{
  StatusType ercd;

//  ercd = SignalCounter(SysTimerCnt); /* Increment OSEK Alarm Counter */
//  if(ercd != E_OK)
//  {
//    ShutdownOS(ercd);
//  }
  
    check_sleepers(); /* check on sleeping Tasks */
}


int setServo(U8 port_id, motion_t *motion)
{
  int i;
  S16 angle;
  U8 servo_ch;
  
  for (i = 0; i < N_NXTe_CH; i++)
  {
    /* check all servos to be controlled is not moving */
    if (read_NXTeMotion(port_id, NXTe_CH[i]) != 0) return 0;
  }

  for (i = 0; i < N_NXTe_CH; i++)
  {
    /* send data to each NXTe channel(LSC) */
    for (servo_ch = MIN_SERVO_CH; servo_ch <= MAX_SERVO_CH; servo_ch++)
    {
      angle = motion->angle[i][servo_ch-1];
      if (angle != BLANK)
      {
        /* send angle data to each servo */
        set_NXTeAngle(port_id, NXTe_CH[i], servo_ch, angle);
      }
    }
  }

  if (motion->wait > 0)
  { 
    /* OSEK caller task is turned into waiting mode 
     * during the specified period in msec
     */
    sleep(motion->wait);
  }

  return 1;
}


void setMotion(U8 port_id, motionCmd_t cmd)
{
  int i;
  
  if (cmd >= sizeof(motionTable)) return;
  
  for (i = 0; i < motionTable[cmd].steps; i++)
  {
    if (setServo(port_id, motionTable[cmd].pMotion + i) == 0)
    {
      /* if robot is moving, try the same step again */
      if (--i < 0) i = 0;
    }
  }
}

void sleep(unsigned int duration)
{
  int i;
  
  for (i = 0;i < N_SLEEPERS; i++)
  {
    if (!isTaskSleeping[i])
    {
      isTaskSleeping[i] = 1;
      sleepTaskCounter[i] = duration;
      GetTaskID(sleeperTaskID+i);
      vTaskSuspend(NULL);
      //WaitEvent(SleepEventMask);
      //ClearEvent(SleepEventMask);
      break;
    }
    }
}


void check_sleepers(void)
{   
  int i;
  int id;

   /* check on sleeping Tasks */
  for (i = 0; i < N_SLEEPERS; i++)
  {
    if (isTaskSleeping[i])
    {
      if (!--sleepTaskCounter[i])
      {
        id = sleeperTaskID[i];
        isTaskSleeping[i] = 0;
        vTaskResume(Task_MotionControlH);
        //SetEvent(id,SleepEventMask);
      }
    }
  }
}


motionCmd_t getMotionCommand(S8 in1, S8 in2)
{
  motionCmd_t cmd = 10; /* default command */
  
  if (in1 == 0)
  {
    if (in2 > 0)
    {
      cmd = 20;
    }
    else if (in2 < 0)
    {
      cmd = 30;
    }
  }
  else if (in1 > 0)
  {
    if (in2 == 0)
    {
      cmd = 40;
    }
  }

  return cmd;
} 



void vbiped_robot( void )
{
	ResourceCommand = xSemaphoreCreateMutex();
  xTaskCreate( FTask_InitH, "Task_Init", priSTACK_SIZE, NULL, 4, NULL );
  xTaskCreate( FTask_CommanderH, "Task_Commander", priSTACK_SIZE, NULL, 3, NULL );
	xTaskCreate( FTask_DisplayH, "Task_Display", priSTACK_SIZE, NULL, 2, NULL );
	xTaskCreate( FTask_MotionControlH, "Task_MotionControl", priSTACK_SIZE, NULL, 1, NULL );
	
}
/*-----------------------------------------------------------*/

static portTASK_FUNCTION(FTask_InitH, pvParameters )
{
  lock("P");
  initServo(NXTe_PORT);
  motionCmd = 10; /* default motion */
  //motionCmd = STAND_UP; /* default motion */
  
  vTaskDelete(NULL);
}


static portTASK_FUNCTION(FTask_CommanderH, pvParameters )
{
  vTaskDelay("1");
  lock("P");
  while(1)
  {
    U8 bt_receive_buf[32]; /* buffer size is fixed as 32 */ 
  
    /* read Bluetooth Rx data */
      if (ecrobot_read_bt_packet(bt_receive_buf, 32))
      {
        if( xSemaphoreTake( ResourceCommand, portMAX_DELAY ) == pdPASS )
        {
          //motionCmd = 10;
          motionCmd = getMotionCommand(((S8)0), ((S8)0));
          xSemaphoreGive( ResourceCommand);
        }
      }
      vTaskDelay("5");
  }
  vTaskDelete(NULL);
}

static portTASK_FUNCTION(FTask_DisplayH, pvParameters )
{
  lock("P");
  while(1)
  {
    ecrobot_status_monitor("BIPED ROBOT");
  }
  vTaskDelete(NULL);
}

static portTASK_FUNCTION(FTask_MotionControlH, pvParameters )
{
  lock("P");
  while(1)
  {
    motionCmd_t cmd;
    
      if( xSemaphoreTake( ResourceCommand, portMAX_DELAY ) == pdPASS )
      {
        cmd = motionCmd; /* latch motion command */
        xSemaphoreGive( ResourceCommand);
      }
    
      /* set a robot motion */
      setMotion(NXTe_PORT, cmd);
    
  }
}
