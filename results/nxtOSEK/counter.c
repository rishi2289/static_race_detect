/*
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --im_ceil=y --eq_fcfs=y --domay_race counter.c 2> counter.txt
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>



/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define _XOPEN_SOURCE 500
#define E_OK 0


typedef struct{
	int x;
	int y;
}point;

point p = {0, 0};


static portTASK_FUNCTION_PROTO( FPosixSignal_USR2_ISRH, pvParameters );

static portTASK_FUNCTION_PROTO( Fprinter_ISRH, pvParameters );

static portTASK_FUNCTION_PROTO( FshutdownH, pvParameters );



/* Demo task specific constants. */
#define priSTACK_SIZE				( configMINIMAL_STACK_SIZE )
#define priSLEEP_TIME				pdMS_TO_TICKS( 128 )
#define priLOOPS					( 5 )
#define priMAX_COUNT				( ( uint32_t ) 0xff )
#define priNO_BLOCK					( ( TickType_t ) 0 )
#define priSUSPENDED_QUEUE_LENGTH	( 1 )

SemaphoreHandle_t xMutex1;


void lock(const char *s)
{
	;
}

void unlock(const char *s)
{
	;
}

void vStartcounter( void )
{
	xMutex1 = xSemaphoreCreateMutex();
	xTaskCreate( FPosixSignal_USR2_ISRH, "ISR_for_PosixSignal_USR2", priSTACK_SIZE, NULL, 30, NULL );
	xTaskCreate( Fprinter_ISRH, "ISR_for_printer", priSTACK_SIZE, NULL, 2, NULL );
	xTaskCreate( FshutdownH, "task_for_FshutdownH", priSTACK_SIZE, NULL, 3, NULL );
	
}
/*-----------------------------------------------------------*/


static portTASK_FUNCTION(FPosixSignal_USR2_ISRH, pvParameters )
{
        lock("I");
        p.x ++;
        p.y ++;
}


static portTASK_FUNCTION(Fprinter_ISRH, pvParameters )
{
	if( xSemaphoreTake( xMutex1, portMAX_DELAY ) == pdPASS )
	{
       //GetResource(Mutex);
       printf("%i, %i\n", p.x, p.y);
       p.x = 0;
       p.y = 0;
       xSemaphoreGive( xMutex1);
       //ReleaseResource(Mutex);
   }
}

static portTASK_FUNCTION(FshutdownH, pvParameters )
{
    vTaskDelay(10);
    printf("Shutting down...");
    //FshutdownHOS(E_OK);
    enter_CRITICAL();
    vTaskDelete(NULL);
    //TerminateTask();
}
