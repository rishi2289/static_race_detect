/*
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --im_ceil=y --eq_fcfs=y --domay_race pingpong.c 2> pingpong.txt
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"


#define _XOPEN_SOURCE 500
#define E_OK 0


static portTASK_FUNCTION_PROTO( FpingISRH, pvParameters );

static portTASK_FUNCTION_PROTO( FpongISRH, pvParameters );

static portTASK_FUNCTION_PROTO( FshutdownH, pvParameters );



/* Demo task specific constants. */
#define priSTACK_SIZE				( configMINIMAL_STACK_SIZE )
#define priSLEEP_TIME				pdMS_TO_TICKS( 128 )
#define priLOOPS					( 5 )
#define priMAX_COUNT				( ( uint32_t ) 0xff )
#define priNO_BLOCK					( ( TickType_t ) 0 )
#define priSUSPENDED_QUEUE_LENGTH	( 1 )

SemaphoreHandle_t xMutex1;
char* cubbyHole = "pong";



void lock(const char *s)
{
	;
}

void unlock(const char *s)
{
	;
}



/*-----------------------------------------------------------*/
/*
 * Start the three tasks as described at the top of the file.
 * Note that the limited count task is given a higher priority.
 */
void vStartpingpong( void )
{
	xMutex1 = xSemaphoreCreateMutex();
	xTaskCreate( FpingISRH, "ISR_for_ping", priSTACK_SIZE, NULL, 1, NULL );
	xTaskCreate( FpongISRH, "ISR_for_pong", priSTACK_SIZE, NULL, 2, NULL );
	xTaskCreate( FshutdownH, "task_for_FshutdownH", priSTACK_SIZE, NULL, 3, NULL );
	
}
/*-----------------------------------------------------------*/


static portTASK_FUNCTION(FpingISRH, pvParameters )
{
    lock("P");
    printf("Ping started.\n");
    while(1)
    {
    	if( xSemaphoreTake( xMutex1, portMAX_DELAY ) == pdPASS )
    	{
            //GetResource(cubbyHoleMutex);
    		cubbyHole = "ping";
        	printf("Current state is: %s\n", cubbyHole);
        	xSemaphoreGive( xMutex1);
        	//ReleaseResource(cubbyHoleMutex);
    	}
    }
    //vTaskDelete(NULL);
}


static portTASK_FUNCTION(FpongISRH, pvParameters )
{
    lock("P");
    printf("Pong started.\n");
    while(1)
    {
    	if( xSemaphoreTake( xMutex1, portMAX_DELAY ) == pdPASS )
    	{
    		//GetResource(cubbyHoleMutex);
        	cubbyHole = "pong";
        	printf("Current state is: %s\n", cubbyHole);
        	xSemaphoreGive( xMutex1);
        	//ReleaseResource(cubbyHoleMutex);
    	}
    }
}

static portTASK_FUNCTION(FshutdownH, pvParameters )
{
    lock("P");
    vTaskDelay(100);
    printf("Shutting down...");
    enter_CRITICAL();
    vTaskDelete(NULL);
    //FshutdownHOS(E_OK);
}
