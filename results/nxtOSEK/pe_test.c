/*
cilly -c -w --save-temps -I include -I FreeRTOS/Source/include -I FreeRTOS/Demo/IA32_flat_GCC_Galileo_Gen_2 -I FreeRTOS/Source/portable/GCC/IA32_flat --im_ceil=y --eq_fcfs=y --domay_race pe_test.c 2> pe_test.txt
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"


static portTASK_FUNCTION_PROTO( FLowTaskH, pvParameters );
static portTASK_FUNCTION_PROTO( FHighTaskH, pvParameters );
SemaphoreHandle_t lcd;
#define priSTACK_SIZE       ( configMINIMAL_STACK_SIZE )


int digits;


void lock(const char *s)
{
	;
}

void unlock(const char *s)
{
	;
}

void vpetest( void )
{
	lcd = xSemaphoreCreateMutex();
	xTaskCreate( FLowTaskH, "LowTask", priSTACK_SIZE, NULL, 2, NULL );
	xTaskCreate( FHighTaskH, "HighTask", priSTACK_SIZE, NULL, 3, NULL );
	
}
/*-----------------------------------------------------------*/

static portTASK_FUNCTION(FLowTaskH, pvParameters )
{
  lock("P");
  while(1)
  {
    int hcount, lcount;

    for (hcount = 0; hcount < 10; hcount++) {
      digits--;
      if( xSemaphoreTake( lcd, portMAX_DELAY ) == pdPASS )
      {
        //GetResource(lcd);

        ecrobot_debug1(digits, hcount, 0);
        /* The following loop is to wait for the ISR triggered by ecrobot_debug
         * (or the specific I/O statement inside ecrobot_debug) to complete
         * This is a good example to show how explicit/implicit shared resource
         * is needed to be protected against concurrent accesses from multiple Tasks
       */
        for (lcount = 0; lcount < 3200; lcount++) ;

        xSemaphoreGive( lcd);
        //ReleaseResource(mx);
      }
    }
  }
}


static portTASK_FUNCTION(FHighTaskH, pvParameters )
{
  lock("P");
  vTaskDelay(3000);
  while(1) {
    int hcount, lcount;

    for (hcount = 0; hcount < 10; hcount++) {
      if( xSemaphoreTake( lcd, portMAX_DELAY ) == pdPASS )
      {
        //GetResource(lcd);

        ecrobot_debug2(digits, hcount, 0);
          /* The following loop is to wait for the ISR triggered by ecrobot_debug
           * (or the specific I/O statement inside ecrobot_debug) to complete
           * This is a good example to show how explicit/implicit shared resource
           * is needed to be protected against concurrent accesses from multiple Tasks
         */
      for (lcount = 0; lcount < 3200; lcount++) ;
      xSemaphoreGive( lcd);
      //ReleaseResource(mx);
      }
    }
    digits=0;

  }
}
