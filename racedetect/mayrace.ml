(*Included Modules*)
open Cil
open Cfg
open Feature
open Pretty
open Printf
 
module DF = Dataflow
module E  = Errormsg
module H  = Hashtbl
module IH = Inthash
module L  = List
module S  = String
module I64 = Int64
module P = Pervasives



type funT = { mutable funName : string }
let currFun =  { funName  = "current"}
let globvars : string list ref = ref []
let wrvars : string list ref = ref []
let alvars : (string * string) list ref = ref []


type minmax = int64 * int64
type ptable = ( string, minmax list) H.t
type strtable = ( string, (string * int64) list) H.t
let init_prty: ptable = H.create 16
let init_stabl : strtable = H.create 16
let init_rtabl : strtable = H.create 16
let init_ftabl : strtable = H.create 16
let temp : (int64 * int64) ref = ref (I64.zero,I64.zero)
let temp1 : int64 ref = ref I64.zero
let mutexlist : (string * int64) list ref = ref []
let currmutex : (string * string) ref = ref ("NULL","NULL")
	
type somtyp = int * location * string * string * string * minmax * (string list)
let outlist : somtyp list ref = ref []
let b_int_a : bool ref = ref true
let a_int_b : bool ref = ref true
let tmp1 : bool ref = ref false
let count : int ref = ref 0
let pdrcnt : int ref = ref 0
let drfcnt : int ref = ref 0
let s_tim : float ref = ref 0.0
let im_ceil : string ref = ref "NULL"
let eq_fcfs : string ref = ref "NULL"
let bloking : string list ref = ref []
let flag_list : string list ref = ref []


let (|>) (a : 'a) (f : 'a -> 'b) : 'b = f a


(*LOCKSET ANALYSIS*)

(*Dataflow value*)

type lock = string

let lock_list_pretty () (ll : lock list) =
  text (S.concat " " ll) 

(*List functions*)
let dupl_h a b =
  if a = b then true else false


let dupl_m s1 (s2,_) =
  if s1 = s2 then true else false


let fun_task s1 =
  if (S.get s1 ((S.length s1)-1)) = 'H' then ( S.sub s1 1 ((S.length s1)-1))
else (S.sub s1 1 ((S.length s1)-2))

let add list1 val1 = 
  val1::list1

let rec add_all list1 list2 =
  match list2 with
  | [] ->  list1
  | Const(CStr(v1))::list3 -> add_all (add list1 v1) list3

let rec del list1 val1 =
  match val1 with
  | x when (L.exists (dupl_h x) !globvars) -> 
  ( match list1 with
  | val2::list2 when val2 = val1 -> (del list2 val1)
  | val2::list2 -> val2::(del list2 val1)
  | _ -> [] )
  | _ -> 
  ( match list1 with
  | val2::list2 when val2 = val1 -> list2
  | val2::list2 -> val2::(del list2 val1)
  | _ -> [] )
  (*match list1 with
  [] -> []
  |val2::list2 when (val2 = val1 && (val1 = A || val1 = D)) -> list2
  |val2::list2 -> val2::(del list2 val1) *)

let rec del_all list1 list2 =
  match list2 with
  | [] -> list1
  | Const(CStr(v1))::list3 -> del_all (del list1 v1 ) list3
  | Lval(Var(x),_)::list3 when (L.exists (dupl_m x.vname) !mutexlist) -> (del list1 x.vname)
  | _ -> list1


let rec present list1 val1 =
  match list1 with
  | [] -> false
  | val2::list2 when val2 = val1 -> true
  | val2::list2 -> present list2 val1 

let rec intersection list1 list2 =
  match list1 with 
  | [] -> []
  | val2::list3 when present list2 val2 -> val2::( intersection list3 (del list2 val2) )
  | val2::list3 -> intersection list3 list2 

let instr_action_l (instr1: instr) (list1: lock list) : lock list =
  match instr1 with
  | Call(lval1,expr1,exprl,loc) -> 
    ( match expr1 with
      | Lval((Var(v1), _ )) when v1.vname = "lock" -> add_all list1 exprl
      | Lval((Var(v1), _ )) when v1.vname = "unlock" -> del_all list1 exprl
(*      | Lval(Var(v1), _ ) when v1.vname = "vTaskSuspend" -> *)
      | Lval(Var(v1), _ ) when v1.vname = "xQueueSemaphoreTake" -> 
      ( match exprl with
        | Lval(Var(x),_)::list3 when (L.exists (dupl_m x.vname) !mutexlist) -> 
        ( match lval1 with
          | Some (Var(var5),_) ->
          ( currmutex := (var5.vname, x.vname);
            (*E.log "\n%a   %s -> %s\n" d_loc !currentLoc var5.vname x.vname;*)
        list1 )
        | _ -> list1 )
      | _ -> list1 )
      | Lval(Var(v1), _ ) when v1.vname = "xQueueGenericSend" -> del_all list1 exprl
      | _ -> list1 )
  | _ -> list1  


let guard_action_l exp1 list1 = 
  (*E.log "\n%a guard_action_l\n" d_loc !currentLoc;*)
  match exp1 with
  | BinOp(Eq,expl,_,_) -> 
  ( (*E.log "\n%a == FOUND!!!!!!!!!!!!\n" d_loc (!currentLoc);*)
    ( match expl with
      | Lval( Var(v1),_ ) ->
      ( (*E.log "\n%a %s FOUND!!!!!!!!!!!!\n" d_loc (!currentLoc) v1.vname;*)
        if ( (P.fst !currmutex) = v1.vname ) then 
        ( (*E.log "\nSUCCESSFUL\n";*)
          (P.snd !currmutex)::list1 ) 
      else list1 )
      | _ -> list1 ) )
  | _ ->
  ( (*E.log "\n%a NO Match!\n" d_loc (!currentLoc);*)
    list1 )



(*Dataflow engine*)
module LocksetDF = struct

  let name = "Lockset"
  let debug = ref false
  type t = lock list
  let copy list1 = list1
  let stmtStartData = IH.create 64
  let pretty = lock_list_pretty

  let computeFirstPredecessor (stmt1: stmt) (list1: t) = list1

  let combinePredecessors (stmt1: stmt) ~(old: t) (new1: t) =
    if old = new1 then None else
    Some(intersection old new1)

  let doInstr (instr1: instr) (list1: t) = 
    let action = instr_action_l instr1 in
    DF.Post action

  let doStmt (stmt1: stmt) (list1: t) = DF.SDefault

  let doGuard (exp1: exp) (list1: t) = 
  let gaction = guard_action_l exp1 list1 in
  DF.GUse gaction

  let filterStmt (stmt1: stmt) = true

end

module Lockset = DF.ForwardsDataFlow(LocksetDF)


(*Algorithm*)
let computelockset (fd: fundec) : unit =
  clearCFGinfo fd;
  ignore(cfgFun fd);
  let initstmt = L.hd fd.sbody.bstmts in
  let init = [] in
  IH.clear LocksetDF.stmtStartData;
  IH.add LocksetDF.stmtStartData initstmt.sid init;
  Lockset.compute [initstmt]

(*Initial functions*)
let onlyforfn (fn : fundec -> location -> unit) (g : global) : unit = 
  match g with
  | GFun(f, loc) -> fn f loc
  | _ -> ()

(*Visitor*)

let getLockSet (sid : int) : lock list option =
  try Some(IH.find LocksetDF.stmtStartData sid)
  with Not_found -> None


let instrLockSet (il : instr list) (ll : lock list) : lock list list =
  let proc_one hil i =
    match hil with
    | [] -> (instr_action_l i ll) :: hil
    | ll':: rest as l -> (instr_action_l i ll') :: l
  in
  il |> L.fold_left proc_one [ll] |> L.tl |> L.rev 

class locksetclass = object(self)
  inherit nopCilVisitor

  val mutable sid           = -1
  val mutable state_list    = []
  val mutable current_state = None

  method vstmt stm =
    sid <- stm.sid;
    (*E.log "%s%d\n" "locksetclass-vtsmt: " sid;*)
    begin match getLockSet sid with
    | None -> current_state <- None
    | Some ll -> begin
      match stm.skind with
      | Instr il ->
        current_state <- None;
        state_list <- instrLockSet il ll 
      | _ -> current_state <- None
    end end;
    DoChildren

  method vinst i = 
  (*E.log "%s\n" "locksetclass-vinst";*)
    try let data = L.hd state_list in
        current_state <- Some(data);
        state_list <- L.tl state_list;
        E.log "%a %a\n" d_loc (!currentLoc) lock_list_pretty data;
        DoChildren
    with Failure "hd" -> DoChildren

  method get_cur_ll () =
    match current_state with
    | None -> getLockSet sid
    | Some ll -> Some ll



  (*method vlval (vl : lval) =
    match self#get_cur_ll () with
    | None -> SkipChildren
    | Some ll ->  
  begin   
    match vl with
        (Var(vi), NoOffset) when (L.exists (dupl_h vi.vname) !globvars) -> E.log "%a %a %a\n"  d_lval vl lock_list_pretty ll d_loc (!currentLoc)
(*        (Var(vi), NoOffset) when vi.vname = "abc" -> E.log "\n%a%a;%s;%a" d_loc (!currentLoc) d_lval vl currFun.funName lock_list_pretty ll
      | (Var(vi), NoOffset) when vi.vname = "pqr" -> E.log "\n%a%a;%s;%a" d_loc (!currentLoc) d_lval vl currFun.funName lock_list_pretty ll*)
      | _ -> E.log "" 
        end;
  
  SkipChildren*)

end


let globscan g =
  match g with
  | GVar(v1,_,_) when v1.vglob = true ->  
  ( ignore(globvars := L.append !globvars [v1.vname]); 
    (*E.log "%s  %a\n" v1.vname d_loc (!currentLoc)*) )
  | _ -> ()



(*PRIORITY ANALYSIS*)

(*Utilities*)


let m2s (k : minmax) : string =
  match k with
  | (i,j) -> S.concat "," [(I64.to_string i);(I64.to_string j)]
  | _ -> "NULL"

let m2s_list (ml : minmax list) : string =
  ml |> L.map m2s |> String.concat "::"

let minmax_list_pretty () (ml : minmax list) =
  ml |> m2s_list |> text

let less a b =
	if a<b then a else b

let more a b =
	if a<b then b else a

let dupl_p a b =
  match b with
  | (c,d) -> if a = c && a = d then true else false
  | _ -> false


let listmid list1 elem1 =
  elem1::L.tl list1

let listadd list1 elem1 =
  L.hd list1 :: listmid list1 elem1

let list_add elem1 list1 =
  elem1::list1

let pcomp a b =
  match a with
  | (c,d) -> 
  ( match b with
    | (e,f) -> if c>e then
    ( if d>f then (e,d) else (e,f)) else 
    (if d>f then (c,d) else (c,f))
    | _ -> a )
  | _ -> b

let get_H a =
  if (S.get a ((S.length a)-1)) = 'H' then (a)
else (S.sub a 0 ((S.length a)-2))

let iterate_p (key1: string) (value1: minmax list)=
  E.log "%s->%a\n" key1 minmax_list_pretty value1

let iterate_t pair1 =
  E.log "%s %d;" (fst pair1) (i64_to_int (snd pair1))

let iterate_h (key1: string) (value1: (string * int64) list)=
  E.log "%s-> " key1;
  L.iter iterate_t value1;
  E.log "\n"


(*Processing functions*)

let create_action instr1 =
  match instr1 with
  | Call(lval1,expr1,exprl,loc) ->
  ( match expr1 with
    | Lval(Var(v1), _ ) when v1.vname = "xTaskCreate" ->
    ( match ((L.nth exprl 0), (L.nth exprl 4)) with 
      | (AddrOf(Var(x),NoOffset), Const(CInt64(v2,_, _)) ) -> 
       (*E.log "%s %s %a\n" "Case 1" x.vname d_loc (!currentLoc);*)
      H.add init_prty (S.sub x.vname 1 ((S.length x.vname)-1)) [(v2 ,v2)]
      | (AddrOf(Var(x),NoOffset), CastE(TNamed(xyz,_),e1) )  -> 
      ( match (xyz.ttype,e1) with
        | (TInt(_),Const(CInt64(v2,_, _))) -> 
        (*E.log "%s %s %a\n" "Case 2" x.vname d_loc (!currentLoc);*)
        H.add init_prty (S.sub x.vname 1 ((S.length x.vname)-1)) [(v2,v2)]
        | _ -> () )
      | _ -> () )
    | Lval(Var(v1), _ ) when v1.vname = "xQueueCreateMutex" -> 
    ( match lval1 with 
      | Some (Var(vbl),_) -> mutexlist := L.append !mutexlist [(vbl.vname,I64.zero)] )
    | _ -> () )
  | _ -> ()


let change_priority expl1 =
  match ((L.nth expl1 0), (L.nth expl1 1)) with
  | (Lval(Var(x),_), Const(CInt64(v2,_, _))) ->
  (*E.log "%s\n" "CASE 1";*)
  (if (L.exists (dupl_p v2) (H.find init_prty (get_H x.vname))) then 
    ( (*E.log "%s%s\n" "Duplicate exists for case 1 with priority: " (I64.to_string v2);*)
      () ) 
  else 
  ( (*E.log "%s%s\n" "Duplicate DOES NOT exist for case 1 with priority: " (I64.to_string v2);*)
    H.replace init_prty (get_H x.vname) (listadd (H.find init_prty (get_H x.vname)) (v2,v2)) ) )
  | (Lval(Var(x),_), CastE(TNamed(xyz,_),e1)) ->
  ( match (xyz.ttype,e1) with 
    | (TInt(_),Const(CInt64(v2,_, _))) -> 
    (*E.log "%s%s\n" "CASE 2: " (get_H x.vname);*)
    ( if (L.exists (dupl_p v2) (H.find init_prty (get_H x.vname))) then
      ( (*E.log "%s%s\n" "Duplicate exists for case 2 with priority: " (I64.to_string v2);*)
      () )
    else
    ( (*E.log "%s%s\n" "Duplicate DOES NOT exist for case 2 with priority: " (I64.to_string v2);*)
    H.replace init_prty (get_H x.vname) (listadd (H.find init_prty (get_H x.vname)) (v2,v2)) ) )
    | _ -> () )
  | _ -> ()

let dupl_t a b c =
  if (((P.snd c) = a) && ((P.fst c) = b)) then true else false


let add_handle p1 expl1 a =
  match ((L.hd expl1),a) with
  | (Lval(Var(x),_), 0) ->
  (*E.log "%s\n" "add_handle for suspend";*)
  ( if (H.mem init_stabl (get_H x.vname)) then
    ( if (L.exists (dupl_t p1 (S.sub currFun.funName 1 ((S.length currFun.funName)-1))) (H.find init_stabl (get_H x.vname))) then 
    ( (*E.log "%s%s\n" "Duplicate exists for suspend with handle: " (S.sub currFun.funName 1 ((S.length currFun.funName)-1));*)
      () ) 
  else 
  ( (*E.log "%s%s\n" "Duplicate DOES NOT exist for suspend with handle: " (S.sub currFun.funName 1 ((S.length currFun.funName)-1));*)
    H.replace init_stabl (get_H x.vname) ( ((S.sub currFun.funName 1 ((S.length currFun.funName)-1)), p1)::(H.find init_stabl (get_H x.vname)) ) ) )
  else 
  ( H.add init_stabl (get_H x.vname) [((S.sub currFun.funName 1 ((S.length currFun.funName)-1)), p1)] ) )
  | (Lval(Var(x),_), 1) ->
  (*E.log "%s\n" "add_handle for resume";*)
  ( if (H.mem init_rtabl (get_H x.vname)) then
    ( if (L.exists (dupl_t p1 (S.sub currFun.funName 1 ((S.length currFun.funName)-1))) (H.find init_rtabl (get_H x.vname))) then 
    ( (*E.log "%s%s\n" "Duplicate exists for resume with handle: " (S.sub currFun.funName 1 ((S.length currFun.funName)-1));*)
      () ) 
  else 
  ( (*E.log "%s%s\n" "Duplicate DOES NOT exist for resume with handle: " (S.sub currFun.funName 1 ((S.length currFun.funName)-1));*)
    H.replace init_rtabl (get_H x.vname) ( ((S.sub currFun.funName 1 ((S.length currFun.funName)-1)), p1)::(H.find init_rtabl (get_H x.vname))) ) )
  else 
  ( H.add init_rtabl (get_H x.vname) [((S.sub currFun.funName 1 ((S.length currFun.funName)-1)), p1)] ) )
  | (_, 0) -> 
  ( if (H.mem init_stabl (S.sub currFun.funName 1 ((S.length currFun.funName)-1))) then
  	( H.replace init_stabl (S.sub currFun.funName 1 ((S.length currFun.funName)-1)) ( ("NULL", p1)::(H.find init_stabl (S.sub currFun.funName 1 ((S.length currFun.funName)-1)) ) ) )
  else
  ( H.add init_stabl (S.sub currFun.funName 1 ((S.length currFun.funName)-1)) [("NULL", p1)] ) )
  | (Const(CStr(flname)), 2) ->
  if ((S.length flname) > 4) then
  (  (* E.log "Found unlock() with %s\n" (S.sub flname 4 ((S.length flname)- 4)); *)
  if L.exists (dupl_h (S.sub flname 4 ((S.length flname)- 4))) !flag_list then
  ( (* E.log "Found %s in flag_list\n" (S.sub flname 4 ((S.length flname)- 4)); *)
  if H.mem init_ftabl (S.sub flname 4 ((S.length flname)- 4)) then 
  ( (* E.log "Row for %s exists in init_ftabl- REPLACING\n" (S.sub flname 4 ((S.length flname)- 4)); *)
  H.replace init_ftabl (S.sub flname 4 ((S.length flname)- 4)) ( ((S.sub currFun.funName 1 ((S.length currFun.funName)-1)), p1)::(H.find init_ftabl (S.sub flname 4 ((S.length flname)- 4)))) ) 
  else 
  ( (* E.log "Row for %s DOES NOT exist in init_ftabl- ADDING\n" (S.sub flname 4 ((S.length flname)- 4)); *)
    H.add init_ftabl (S.sub flname 4 ((S.length flname)- 4)) [((S.sub currFun.funName 1 ((S.length currFun.funName)-1)), p1)]  ) )
  (* else
  ( E.log "COULD NOT find %s in flag_list\n" (S.sub flname 4 ((S.length flname)- 4));) *) )
  | _ -> ()


(*
BLOCK/WAIT/DELAY:
xTaskAbortDelay
vTaskDelay
vTaskDelayUntil
taskYIELD
ulTaskNotifyTake
xTaskNotifyWait
xQueueSend
xQueueSendToBack
xQueueSendToFront
xQueueReceive
xQueuePeek
xQueueSelectFromSet
xStreamBufferSend
xStreamBufferReceive
xMessageBufferSend
xMessageBufferReceive
xSemaphoreTake
xSemaphoreTakeRecursive
xTimerStart
xTimerStop
xTimerChangePeriod
xEventGroupWaitBits

*)



let wr_action instr1 =
  match instr1 with
  | Set((Var(v1),_),_,_) when ((L.exists (dupl_h v1.vname) !globvars) && not(L.exists (dupl_h v1.vname) !flag_list)   ) -> wrvars := L.append !wrvars [v1.vname]
  | Call(lval1,Lval(Var(v1), _ ),exprl,loc) when ( ( v1.vname = "vTaskDelay" ) || ( v1.vname = "xTaskAbortDelay" ) || ( v1.vname = "vTaskDelayUntil" ) || ( v1.vname = "taskYIELD" ) || ( v1.vname = "ulTaskNotifyTake" ) || ( v1.vname = "xTaskNotifyWait" )  ) -> bloking := L.append !bloking [(fun_task currFun.funName)]
  | Call(lval1,Lval(Var(v1), _ ),exprl,loc) when ( ( v1.vname = "xQueueSend" ) || ( v1.vname = "xQueueSendToBack" ) || ( v1.vname = "xQueueSendToFront" ) || ( v1.vname = "xQueueReceive" ) || ( v1.vname = "xQueuePeek" ) || ( v1.vname = "xQueueSelectFromSet" )  ) -> bloking := L.append !bloking [(fun_task currFun.funName)]
  | Call(lval1,Lval(Var(v1), _ ),exprl,loc) when ( ( v1.vname = "xStreamBufferSend" ) || ( v1.vname = "xStreamBufferReceive" ) || ( v1.vname = "xSemaphoreTake" ) || ( v1.vname = "xSemaphoreTakeRecursive" ) || ( v1.vname = "xMessageBufferSend" ) || ( v1.vname = "xMessageBufferReceive" )  ) -> bloking := L.append !bloking [(fun_task currFun.funName)]
  | Call(lval1,Lval(Var(v1), _ ),exprl,loc) when ( ( v1.vname = "xTimerStart" ) || ( v1.vname = "xTimerStop" ) || ( v1.vname = "xTimerChangePeriod" ) || ( v1.vname = "xEventGroupWaitBits" ) ) -> bloking := L.append !bloking [currFun.funName]
  | _ -> () 

(*let rd_action instr1 =
  match instr1 with
  | Set( _,expr1,loc) -> (*ignore(E.log "%s %a\n" "Binop found" d_loc (!currentLoc))*)
  ( match expr1 with
  | BinOp(_,Lval(Var(v1),_),Lval(Var(v2),_),_) -> ignore(E.log "%s%s %s %a\n" "Binop: " v1.vname v2.vname d_loc (!currentLoc))
  | BinOp(_,Lval(Var(v1),_),_,_) -> ignore(E.log "%s%s  %a\n" "Binop: " v1.vname d_loc (!currentLoc))
  | BinOp(_,_,Lval(Var(v1),_),_) -> ignore(E.log "%s%s  %a\n" "Binop: " v1.vname d_loc (!currentLoc))
  | Lval(Var(v1),_) -> ignore(E.log "%s%s  %a\n" "Assignment: " v1.vname d_loc (!currentLoc))
  | _ -> () )
  | _ -> () 
*)

let al_dupl p1 p2 =
  if p1 = p2 then true else false


let al_iter name1 val1 = 
  ( if (L.exists (dupl_h val1.vname) !wrvars) then
  ( if (L.exists (al_dupl (name1,val1.vname)) !alvars) 
    then ()
  else ( alvars := L.append !alvars [(name1,val1.vname)];
  wrvars := L.append !wrvars [name1]; ) )
else () ) 
  (*E.log "%s " val1.vname*)


let al_action val1 = 
  try let list2= Ptranal.resolve_lval (var val1) in
    ( match list2 with
    | [] -> () (*E.log "%s %a\n" "CHECK" d_loc (!currentLoc) *)
    | list1 -> 
    ( L.iter (al_iter val1.vname) list1
    (*E.log "%a %s\n" d_loc (!currentLoc) val1.vname*) )
    (* match val1 with
    | (Var(v1),_) -> 
    ( L.iter al_iter list1;
    E.log "%a %s\n" d_loc (!currentLoc) v1.vname )
    | _ -> 
    ( L.iter al_iter list1;
    E.log "%a\n" d_loc (!currentLoc) ) *)
    | _ -> () (*E.log "%s %a\n" "EMPTY" d_loc (!currentLoc) *) )
  with Not_found -> ()

(*let addmutexp v1 s1 = *)

let mutexaddp expl1 = 
  match (L.nth expl1 0) with
  | Lval(Var(x),_) when (L.exists (dupl_m x.vname) !mutexlist) -> 
  ( temp1 := P.fst (L.hd (H.find init_prty (S.sub currFun.funName 1 ((S.length currFun.funName)-1)) ) );
    mutexlist := L.append !mutexlist [(x.vname,!temp1)] )
  | _ -> ()


let change_action instr1 =
  match instr1 with
  | Call(lval1,expr1,exprl,loc) -> 
  ( match expr1 with 
    | Lval(Var(v1), _ ) when v1.vname = "vTaskPrioritySet" -> 
    (*E.log "%s %a\n" "vTaskPrioritySet() found" d_loc (!currentLoc);*)
    change_priority exprl 
    | Lval(Var(v1), _ ) when v1.vname = "xQueueSemaphoreTake" -> mutexaddp exprl
    (*| Lval(Var(v1), _ ) when v1.vname = "vTaskSuspend" -> 
        (*E.log "%s %a\n" "vTaskSuspend() found" d_loc (!currentLoc);*)
        add_handle exprl 0
        | Lval(Var(v1), _ ) when v1.vname = "vTaskResume" -> 
        (*E.log "%s %a\n" "vTaskResume match found" d_loc (!currentLoc);*)
        add_handle exprl 1*)
    | _ -> () )
  | _ -> () 



let rec minmax_join list1 list2 =
  match list1 with 
  | (v1,v2) :: list3 -> begin
    match list2 with
    | (v3,v4) :: list4 -> ((less v1 v3),(more v2 v4)) :: minmax_join list3 list4
    | _ -> list1
  end
  | _ -> list2


let setpriority list1 expl1 =
  (*E.log "%s %a\n" "vTaskPrioritySet() found" d_loc (!currentLoc);*)
  match ((L.nth expl1 0), (L.nth expl1 1)) with 
  | (Lval(Var(x), _),_) -> list1
  (*| (Lval(Var(x), _),Const(CInt64(v1, _, _))) -> list1
     | (Lval(Var(x), _),CastE(TNamed(xyz,_),e1) ) -> list1*) 
  | (_,Const(CInt64(v1, _, _))) -> begin
    match list1 with
    | (v2,v3) :: list3 -> (v1,v1) :: list3
    | _ -> [(v1,v1)]
  end
  | (_,CastE(TNamed(xyz,_),e1) ) -> begin
    (match (xyz.ttype,e1) with
      | (TInt(_),Const(CInt64(v1,_, _))) -> 
      (match list1 with 
      | (v2,v3) :: list3 -> (v1,v1) :: list3 
      | _ -> [(v1,v1)] ) 
    | _ -> list1 )
  end 
  | _ -> list1

let maxp s1 (s2,v1) = 
  if ((s1 = s2) && (v1 > !temp1)) then (temp1 := v1)

let inheritp list1 expl1 =
  (*E.log "%s %a\n" "vTaskPrioritySet() found" d_loc (!currentLoc);*)
  match (L.nth expl1 0) with 
  | Lval(Var(x), _) when (L.exists (dupl_m x.vname) !mutexlist) -> 
  ( temp1 := I64.zero;
    L.iter (maxp x.vname) !mutexlist;
    match list1 with
    | (v2,v3) :: list3 -> (v2,!temp1) :: list3
    | _ -> [(!temp1,!temp1)] )
  | _ -> list1

let releasep list1 expl1 =
  (*E.log "%s %a\n" "vTaskPrioritySet() found" d_loc (!currentLoc);*)
  match (L.nth expl1 0) with 
  | Lval(Var(x), _) when (L.exists (dupl_m x.vname) !mutexlist) -> 
  ( temp1 := P.fst (L.hd (H.find init_prty (S.sub currFun.funName 1 ((S.length currFun.funName)-1)) ) );
    match list1 with
    | (v2,v3) :: list3 -> (!temp1,!temp1) :: list3
    | _ -> [(!temp1,!temp1)] )
  | _ -> list1




let instr_action_p instr1 list1 =
  match instr1 with
  | Call(lval1,expr1,exprl,loc) -> 
  ( match expr1 with 
    | Lval((Var(v1), _ )) when v1.vname = "vTaskPrioritySet" -> setpriority list1 exprl 
    | Lval((Var(v1), _ )) when v1.vname = "xQueueGenericSend" -> releasep list1 exprl
    | Lval(Var(v1), _ ) when v1.vname = "xQueueSemaphoreTake" -> 
      ( match exprl with
        | Lval(Var(x),_)::list3 when (L.exists (dupl_m x.vname) !mutexlist) -> 
        ( match lval1 with
          | Some (Var(var5),_) ->
          ( currmutex := (var5.vname, x.vname);
            (*E.log "\n%a   %s -> %s\n" d_loc !currentLoc var5.vname x.vname;*)
        list1 )
        | _ -> list1 )
      | _ -> list1 )
    | _ -> list1 )
  | _ -> list1  


let guard_action_p exp1 list1 = 
  (*E.log "\n%a guard_action_p\n" d_loc !currentLoc;*)
  match exp1 with
  | BinOp(Eq,expl,_,_) -> 
  ( (*E.log "\n%a == FOUND!!!!!!!!!!!!\n" d_loc (!currentLoc);*)
    ( match expl with
      | Lval( Var(v1),_ ) ->
      ( (*E.log "\n%a %s FOUND!!!!!!!!!!!!\n" d_loc (!currentLoc) v1.vname;*)
        if ( (P.fst !currmutex) = v1.vname ) then
        ( (*E.log "\nSUCCESSFUL\n";*)
          temp1 := I64.zero;
          L.iter (maxp (P.snd !currmutex)) !mutexlist;
          match list1 with
          | (v2,v3) :: list3 -> if (!im_ceil = "y") then ((!temp1,!temp1) :: list3) else ((v2,!temp1) :: list3) 
          | _ -> [(!temp1,!temp1)] )
      else list1 )
      | _ -> list1 ) )
  | _ ->
  ( (*E.log "\n%a NO Match!\n" d_loc (!currentLoc);*)
    list1 )


(*Dataflow engine for priority analysis*)
module PriorityDF = struct

let name = "priority"
  let debug = ref false
  type t = minmax list
  let copy list1 = list1
  let stmtStartData = IH.create 64
  let pretty = minmax_list_pretty

  let computeFirstPredecessor (stmt1: stmt) (val1: t) = val1

  let combinePredecessors (stmt1: stmt) ~(old: t) (new1: t) =
    if old = new1 then None else
    Some(minmax_join old new1)

  let doInstr (instr1: instr) (list1: t) = 
    let action = instr_action_p instr1 in
    DF.Post action

  let doStmt (stmt1: stmt) (list1: t) = DF.SDefault

  let doGuard (exp1: exp) (list1: t) = 
  let gaction = guard_action_p exp1 list1 in
  DF.GUse gaction

  let filterStmt (stmt1: stmt) = true

end

module Priorityfn = DF.ForwardsDataFlow(PriorityDF)


let computepriority (fd: fundec) : unit =
  let initstmt = L.hd fd.sbody.bstmts in
  let init = H.find init_prty (S.sub fd.svar.vname 1 ((S.length fd.svar.vname)-1)) in
  IH.clear PriorityDF.stmtStartData;
  IH.add PriorityDF.stmtStartData initstmt.sid init;
  Priorityfn.compute [initstmt];
  
  


(*Visitors*)

class initclass = object(self)
  inherit nopCilVisitor

method vinst i =
  (* E.log "%s\n" "initclass vinst invoked";*)
  create_action i;
  DoChildren
end

class contclass = object(self)
  inherit nopCilVisitor

(*method vstmt stm =
  E.log "%s\n" "contclass vstmt invoked";
  DoChildren*)

method vinst i = 
  (*E.log "%s\n" "contclass vinst invoked";*)
  change_action i;
  DoChildren
end

class wrscnclass = object(self)
  inherit nopCilVisitor

method vinst i =
  (*E.log "%s\n" "initclass vinst invoked"; *)
  wr_action i;
  DoChildren
end


class alscanclass =
  object (self)
  inherit nopCilVisitor
  method vvrbl i = 
  (*E.log "%s\n" "vvrbl invoked";*)
  al_action i;
  DoChildren
  end

(*class rdscnclass = object(self)
  inherit nopCilVisitor

method vinst i =
  (*E.log "%s\n" "initclass vinst invoked"; *)
  rd_action i;
  DoChildren
end
*)

let getplist (sid : int) : minmax list option =
  try Some(IH.find PriorityDF.stmtStartData sid)
  with Not_found -> None


let instrplist (il : instr list) (ml : minmax list) : minmax list list =
  let proc_one hil i =
    match hil with
    | [] -> (instr_action_p i ml) :: hil
    | ml':: rest as l -> (instr_action_p i ml') :: l
  in
  il |> L.fold_left proc_one [ml] |> L.tl |> L.rev 


class ptprint =
  object (self)
  inherit nopCilVisitor

  val mutable sid             = -1
  val mutable state_list_l    = []
  val mutable current_state_l = None
  val mutable state_list_p    = []
  val mutable current_state_p = []
  val mutable w_flag          = 0

(* Invoked on Control-flow statement. *)
  method vstmt stm =
    sid <- stm.sid;
    (*E.log "\n%a ptprint-sid: %d\n" d_loc !currentLoc sid;*)
    
    begin match getplist sid with
    | None -> current_state_p <- []
    | Some ml -> begin
      match stm.skind with
      | Instr il ->
        current_state_p <- [];
        state_list_p <- instrplist il ml 
      | _ -> current_state_p <- []
    end end;

    begin match getLockSet sid with
    | None -> current_state_l <- None
    | Some ll -> begin
      match stm.skind with
      | Instr il ->
        current_state_l <- None;
        state_list_l <- instrLockSet il ll 
      | _ -> current_state_l <- None
    end end;
    
    DoChildren

  method iter_alw data_l var1 var2 = 
    ( if (  var1.vname = (P.fst var2) ) then 
      ( if (L.exists (dupl_h (P.snd var2)) !globvars) then
        ( temp := (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p));
          count := ( !count + 1);
          outlist := L.append !outlist [(!count, !currentLoc, (S.sub (get_H currFun.funName) 1 ((S.length (get_H currFun.funName)) - 1) ), (P.snd var2), "W", !temp, data_l )] )
        (*E.log "%a;%s;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) currFun.funName (P.snd var2) "W" (i64_to_int ( P.fst !temp)) (i64_to_int (P.snd !temp)) lock_list_pretty data_l*)
        (*E.log "%a;%s;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) currFun.funName (P.snd var2) "W" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l*) )
  (*else ()*) ) 

(* Invoked on each instruction occurrence. *)
  method vinst i = 
  w_flag <- 0;
 (* E.log "\n%a ptprint-vinst1\n" d_loc !currentLoc;
     (match state_list_p with
            | [] -> E.log "empty state_list_p"
            | _ -> E.log "fine") ;
     E.log "\n%a ptprint-vinst2\n" d_loc !currentLoc; *)
    try let data_p = L.hd state_list_p in
        current_state_p <- data_p;
        state_list_p <- L.tl state_list_p;
        (*E.log "%a\n" minmax_list_pretty current_state_p; *)
        ( try let data_l = L.hd state_list_l in
          current_state_l <- Some(data_l);
          state_list_l <- L.tl state_list_l;

          match i with 
          | Call(lval1,expr1,exprl,loc) -> 
          ( match expr1 with
            | Lval(Var(v1), _ ) when v1.vname = "vTaskSuspend" -> 
            ( temp := (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p));
              (*E.log "%s %a\n" "vTaskSuspend() found" d_loc (!currentLoc);*)
              add_handle (P.snd !temp) exprl 0)
            | Lval(Var(v1), _ ) when v1.vname = "vTaskResume" ->
            ( temp := (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p));
              (*E.log "%s %a\n" "vTaskResume match found" d_loc (!currentLoc);*)
              add_handle (P.snd !temp) exprl 1)
            | Lval(Var(v1), _ ) when v1.vname = "unlock" -> 
            ( temp := (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p));
              (*E.log "%s %a\n" "vTaskSuspend() found" d_loc (!currentLoc);*)
              add_handle (P.snd !temp) exprl 2)
            | _ -> () )
          | Set( (Var(var1), Field(fi, _) ),expr1,_) when (L.exists (dupl_h var1.vname) !wrvars) -> 
          ( if (L.exists (dupl_h var1.vname) !globvars) then
            ( temp := (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p));
              count := ( !count + 1);
              outlist := L.append !outlist [(!count, !currentLoc, (S.sub (get_H currFun.funName) 1 ((S.length (get_H currFun.funName)) - 1) ), (S.concat "." [var1.vname;fi.fname]), "W", !temp, data_l )];
              w_flag <- 1 ); )
          | Set( (Var(var1),_),expr1,_) when (L.exists (dupl_h var1.vname) !wrvars) -> 
          ( if (L.exists (dupl_h var1.vname) !globvars) then
            ( temp := (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p));
              count := ( !count + 1);
              outlist := L.append !outlist [(!count, !currentLoc, (S.sub (get_H currFun.funName) 1 ((S.length (get_H currFun.funName)) - 1) ), var1.vname, "W", !temp, data_l )];
              w_flag <- 1 );
            (*E.log "%a;%s;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) currFun.funName var1.vname "W" (i64_to_int ( P.fst !temp)) (i64_to_int (P.snd !temp)) lock_list_pretty data_l;*)
            (*E.log "%a;%s;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) currFun.funName var1.vname "W" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;*)
            (*w_flag <- 1;*)
            (* match expr1 with
              | BinOp(_,Lval(Var(v1),_),Lval(Var(v2),_),_) when ((L.exists (dupl_h v1.vname) !wrvars) && (L.exists (dupl_h v2.vname) !wrvars)  )-> 
              ( E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v1.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;
                E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v2.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l; )
              | BinOp(_,Lval(Var(v1),_),_,_) when (L.exists (dupl_h v1.vname) !wrvars) -> E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v1.vname "RO" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;
              | BinOp(_,_,Lval(Var(v1),_),_) when (L.exists (dupl_h v1.vname) !wrvars) -> E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v1.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;
              | Lval(Var(v1),_) when (L.exists (dupl_h v1.vname) !wrvars) -> E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v1.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;
              | CastE(_,expr2) -> 
              ( E.log "%s\n" "CastE(_)";
                ( match expr2 with
                  | BinOp(_,Lval(Var(v1),_),Lval(Var(v2),_),_) when ((L.exists (dupl_h v1.vname) !wrvars) && (L.exists (dupl_h v2.vname) !wrvars)  )->
                  ( E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v1.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;
                    E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v2.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l; )
                  | BinOp(_,Lval(Var(v1),_),_,_) when (L.exists (dupl_h v1.vname) !wrvars) -> E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v1.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;
                  | BinOp(_,_,Lval(Var(v1),_),_) when (L.exists (dupl_h v1.vname) !wrvars) -> E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v1.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;
                  | Lval(Var(v1),_) when (L.exists (dupl_h v1.vname) !wrvars) -> E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v1.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;
                  | BinOp(_,CastE(_,Lval(Var(v1),_)),_,_) when (L.exists (dupl_h v1.vname) !wrvars) -> E.log "%a;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) v1.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l; 
                  | _ -> () ; ) )
              | _ -> () ; *) )
          | Set( (Mem(Lval(Var(var1),_) ),_) ,_,_) when (L.exists (dupl_h var1.vname) !wrvars) -> 
          ( L.iter (self#iter_alw data_l var1) !alvars;
            (*E.log "%a;%s;%s;%s;(%d,%d);%a\n" d_loc (!currentLoc) currFun.funName var1.vname "W" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;*)
            w_flag <- 1; )
          | _ -> ();
        (*E.log "%a: (%d,%d) %a\n" d_loc (!currentLoc) (i64_to_int ( P.fst (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd current_state_p) (L.tl current_state_p)))) lock_list_pretty data_l;*)
        
        (*E.log "\n%a %s %a" d_loc (!currentLoc) currFun.funName lock_list_pretty data;*)
      with Failure "hd" -> () );
        DoChildren
    with Failure "hd" -> DoChildren


  method get_cur_ll () =
    match current_state_l with
    | None -> getLockSet sid
    | Some ll -> Some ll

  method get_cur_ml () =
    match current_state_p with
    | [] -> getplist sid
    | ml -> Some ml

  method iter_alr ml ll var1 var2 = 
    ( if ( var1.vname = (P.fst var2) ) 
      then ( if (L.exists (dupl_h (P.snd var2)) !globvars) then 
        ( temp := (L.fold_left pcomp (L.hd ml) (L.tl ml));
          count := ( !count + 1);
          outlist := L.append !outlist [(!count, !currentLoc, (S.sub (get_H currFun.funName) 1 ((S.length (get_H currFun.funName)) - 1) ), (P.snd var2), "R", !temp, ll )] )
        (*E.log "%a;%s;%s;%s;(%d,%d);%a\n"  d_loc (!currentLoc) currFun.funName (P.snd var2) "R" (i64_to_int ( P.fst !temp)) (i64_to_int (P.snd !temp)) lock_list_pretty ll*)
        (*E.log "%a;%s;%s;%s;(%d,%d);%a\n"  d_loc (!currentLoc) currFun.funName (P.snd var2) "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd ml) (L.tl ml)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd ml) (L.tl ml)))) lock_list_pretty ll*) )
  (*else ()*) )


  method vlval (vl : lval) =
  ( if w_flag = 0 then 
    ( match self#get_cur_ml () with
    | None -> SkipChildren
    | Some ml -> 
    ( match self#get_cur_ll () with
    | None -> ();
    | Some ll ->  
  begin   
    match vl with
    | (Var(var1), Field(fi, _) ) when (L.exists (dupl_h var1.vname) !wrvars) -> 
    ( if (L.exists (dupl_h var1.vname) !globvars) then 
      ( temp := (L.fold_left pcomp (L.hd ml) (L.tl ml));
        count := ( !count + 1);
        outlist := L.append !outlist [(!count, !currentLoc, (S.sub (get_H currFun.funName) 1 ((S.length (get_H currFun.funName)) - 1) ), (S.concat "." [var1.vname;fi.fname]), "R", !temp, ll )] ) )
    | (Var(vi), NoOffset) when (L.exists (dupl_h vi.vname) !wrvars) -> 
    ( if (L.exists (dupl_h vi.vname) !globvars) then 
      ( temp := (L.fold_left pcomp (L.hd ml) (L.tl ml));
        count := ( !count + 1);
        outlist := L.append !outlist [(!count, !currentLoc, (S.sub (get_H currFun.funName) 1 ((S.length (get_H currFun.funName)) - 1) ), vi.vname, "R", !temp, ll )] )
      (*E.log "%a;%s;%s;%s;(%d,%d);%a\n"  d_loc (!currentLoc) currFun.funName vi.vname "R" (i64_to_int ( P.fst !temp)) (i64_to_int (P.snd !temp)) lock_list_pretty ll*)
      (*E.log "%a;%s;%s;%s;(%d,%d);%a\n"  d_loc (!currentLoc) currFun.funName vi.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd ml) (L.tl ml)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd ml) (L.tl ml)))) lock_list_pretty ll*) )
    | (Mem(Lval(Var(var1),_) ),_) -> L.iter (self#iter_alr ml ll var1) !alvars;
    | _ -> ()
        end );
  SkipChildren )
else 
  ( w_flag <- 0;
SkipChildren ) )    

(*method vvrbl vl  =
  ( if w_flag = 0 then 
    ( match self#get_cur_ml () with
    | None -> SkipChildren
    | Some ml -> 
    ( match self#get_cur_ll () with
    | None -> ();
    | Some ll ->  
  begin   
    match vl with
    | vi when (L.exists (dupl_h vi.vname) !wrvars) -> E.log "%a;%s;%s;%s;(%d,%d);%a\n"  d_loc (!currentLoc) currFun.funName vi.vname "R" (i64_to_int ( P.fst (L.fold_left pcomp (L.hd ml) (L.tl ml)))) (i64_to_int (P.snd (L.fold_left pcomp (L.hd ml) (L.tl ml)))) lock_list_pretty ll
    | _ -> ()
  end );

  SkipChildren )
else 
  ( w_flag <- 0;
  SkipChildren ) )*)

end


(*Initial functions*)
let onlycreate (fn : fundec -> location -> unit) (g : global) : unit = 
  (*E.log "%s\n" "inside onlycreate";*)
  match g with
  | GFun(f, loc) (*when f.svar.vname = "vStartDynamicPriorityTasks"*) -> 
  (*E.log "\n%s%s\n" "onlycreate for " f.svar.vname;*)
  fn f loc
  | _ -> ()


let onlytsk (fn : fundec -> location -> unit) (g : global) : unit = 
  match g with
  | GFun(f, loc) ->
  ( if H.mem init_prty (S.sub f.svar.vname 1 ((S.length f.svar.vname)-1)) then (fn f loc) else () )
  | _ -> ()


let createscan (fd : fundec) (loc : location) : unit =
  (*E.log "\n\n%s%s\n\n" "createscan for " fd.svar.vname;*)
  let vis = ((new initclass) :> nopCilVisitor) in
  ignore(visitCilFunction vis fd)
  (*H.add init_prty "Sample" [(I64.zero,I64.zero)];*)


let fnscan (fd : fundec) (loc : location) : unit =
  (*E.log "\n\n%s%s\n\n" "fnscan for " fd.svar.vname;*)
  currFun.funName <- fd.svar.vname;
  let vis = ((new contclass) :> nopCilVisitor) in
  ignore(visitCilFunction vis fd)
  (*E.log "%s" "Hello";
  H.add init_prty "Sample" [(I64.zero,I64.zero)];*)

let wrscan (fd : fundec) (loc : location) : unit =
  currFun.funName <- fd.svar.vname;
  (*E.log "\n\n%s%s\n\n" "writescan for " fd.svar.vname;*)
  let vis = ((new wrscnclass) :> nopCilVisitor) in
  ignore(visitCilFunction vis fd)
  (*H.add init_prty "Sample" [(I64.zero,I64.zero)];*)

let alscan (fd : fundec) (loc : location) : unit =
  let vis = ((new alscanclass) :> nopCilVisitor) in
  ignore(visitCilFunction vis fd)
  

(*let cfltscan (fd : fundec) (loc : location) : unit =
  E.log "\n\n%s%s\n\n" "cfltscan for " fd.svar.vname;
  currFun.funName <- fd.svar.vname;
  let vis = ((new rdscnclass) :> nopCilVisitor) in
  ignore(visitCilFunction vis fd)
  (*E.log "%s" "Hello";
  H.add init_prty "Sample" [(I64.zero,I64.zero)];*)
*)

let prtyanlys (fd : fundec) (loc : location) : unit =
  currFun.funName <- fd.svar.vname;
  (*E.log "\n\n%s%s\n\n" "prtyanlys for " fd.svar.vname;*)
  computelockset fd;  
  computepriority fd;
  (*E.log "\n%a COMPUTE\n" d_loc !currentLoc;*)
  let vis =((new ptprint) :> nopCilVisitor) in
  ignore(visitCilFunction vis fd)

  (*let vis2 = ((new locksetclass) :> nopCilVisitor) in
  ignore(visitCilFunction vis2 fd)*)

(*let somefn a =
  ignore(E.log "%s::" a.vname) *)

let geqsus v1 p1 = 
  if ((v1 <= (snd p1)) && ((fst p1)<>"NULL"))  then true else false

let gsus v1 p1 = 
  if ((v1 < (snd p1)) && ((fst p1)<>"NULL")) then true else false


let geqres v1 s1 p1 = 
  if ((s1 <> (fst p1)) && (v1 <= (snd p1))) then true else false

let gres v1 s1 p1 = 
  if ((s1 <> (fst p1)) && (v1 < (snd p1))) then true else false

let lres v1 s1 p1 = 
  if ((s1 <> (fst p1)) && (v1 > (snd p1))) then true else false

let eq_NULL p1 = 
	if ((fst p1)="NULL") then true else false


let set_flag t = 
  if (t = 0) then
    ( (*E.log "\nb_int_a\n";*)
    b_int_a := false )
  else
  ( (*E.log "\na_int_b\n";*)
  a_int_b := false )

let check_I s8 = 
  tmp1 := true;
  ( for x = 0 to ((L.length s8) - 1) do 
    ( if ( (L.nth s8 x) = "I") then ( ignore(tmp1 := false) ) ) 
  done );
  !tmp1


let check_A s8 t1 = 
	(*E.log "\ncheck_A\n";*)
	if ((check_I s8) = true) then
	( set_flag t1
		(*E.log "%s %s\n" "Inside b_int_a with lock A and no INTERRUPT" (string_of_bool !b_int_a)*) ) 

let check_P p1 p2 s1 t1 = 
	(*E.log "\ncheck_P\n";*)
  if ( ((fst p1) > (snd p2)) || ((!eq_fcfs = "y") && ((fst p1) = (snd p2)) )  ) then
    ( (*E.log "eq_fcfs: N\n";*)
    	if (H.mem init_stabl s1 ) then
      ( if ( (!eq_fcfs<>"y") && (L.exists (geqsus (fst p1)) (H.find init_stabl s1) ) ) then () 
    else ( if ( (!eq_fcfs="y") && (L.exists (gsus (fst p1)) (H.find init_stabl s1) ) ) then () else ( set_flag t1 ) ) )
      else
      ( set_flag t1 ) )

let check_B p1 s5 t1 s8 s1 = 
	(*E.log "\ncheck_B\n";*)
  if ((check_I s8) = true) then
  ( if (H.mem init_rtabl s5 ) then
       ( if ( (!eq_fcfs<>"y") && (L.exists (geqres (fst p1) s1) (H.find init_rtabl s5) )) then ((*E.log "Reason Found\n"*)) 
     else ( if ((!eq_fcfs="y") && (L.exists (gres (fst p1) s1) (H.find init_rtabl s5) )) then ((*E.log "Reason Found\n"*)) else ( set_flag t1 ) ) )
       else ( if ((not (L.exists (dupl_h s1) !bloking)) || (not (L.exists (lres (fst p1) s1) (H.find init_rtabl s5)))) 
         	then (set_flag t1) ) )

let check_N p1 s5 t1 s8 s1 = 
  if ( ((check_I s8) = true) && (H.mem init_stabl s5) && (not (L.exists (dupl_h s5) !bloking)) ) then
  ( (*E.log "\na_int_b: %s\n" (string_of_bool !a_int_b);*)
  	if (L.exists eq_NULL (H.find init_stabl s5)) then
   ( (*E.log "\neq_NULL\n";*) 
   	if (H.mem init_rtabl s5 ) then
         ( if  ((!eq_fcfs<>"y") && (L.exists (geqres (fst p1) s1) (H.find init_rtabl s5) )) 
         	then ((*E.log "Reason Found\n"*)) 
         else 
         ( if ((!eq_fcfs="y") && (L.exists (gres (fst p1) s1) (H.find init_rtabl s5) )) 
         	then ((*E.log "Reason Found\n"*))
         else 
         ( if ((not (L.exists (dupl_h s1) !bloking)) || (not (L.exists (lres (fst p1) s1) (H.find init_rtabl s5)))) 
         	then (set_flag t1) ) ) )
     else ( set_flag t1 ) ) )

let check_set_flag p1 flag_name t1 s8 s1 = 
  if (L.exists (dupl_h (S.concat "" ["check_";flag_name])) s8) then
  (if (H.mem init_ftabl flag_name ) then
         ( if  ((!eq_fcfs<>"y") && (L.exists (geqres (fst p1) s1) (H.find init_ftabl flag_name) )) then ((*E.log "Reason Found\n"*)) 
       else ( if ((!eq_fcfs="y") && (L.exists (gres (fst p1) s1) (H.find init_ftabl flag_name) )) then ((*E.log "Reason Found\n"*)) else ( set_flag t1 ) ) )
         else ( set_flag t1 ) )


let check_m s8 mutex1 t1 =
  if ( L.exists (dupl_h mutex1) s8 ) then ( set_flag t1 )

let prnt val1 =
  val1.vname

let iter_out (c1,l1,s1,s2,s3,p1,s4) = 
  E.log "%d %a %s %s %s (%d,%d) %s\n" c1 d_loc l1 s1 s2 s3 (i64_to_int (P.fst p1)) (i64_to_int (P.snd p1)) (S.concat " " s4) 

let prnt_mutex (s1,p1) =
  E.log "\n%s %d\n" s1 (i64_to_int p1)


let mainfn (f : file) : unit =
  s_tim := Sys.time();
  uniqueVarNames f;
  computeFileCFG f;
  (*E.log "\n\n";*)
  iterGlobals f (onlyforfn createscan);
  
  iterGlobals f globscan;
  iterGlobals f (onlytsk wrscan);

  Ptranal.debug_may_aliases := true;
  Ptranal.analyze_file f;
  Ptranal.compute_results false;
  iterGlobals f (onlytsk alscan);
(*    
      E.log "\n%s\n" (S.concat " " !globvars);
      E.log "\n%s\n" (S.concat " " !wrvars);
*)

  iterGlobals f (onlytsk fnscan);

  (*L.iter prnt_mutex !mutexlist;*)
  iterGlobals f (onlytsk prtyanlys);
(*  
  E.log "\n%s\n" "Initial Priorities";
  H.iter iterate_p init_prty;
  E.log "\n%s\n" "Suspend Lists";
  H.iter iterate_h init_stabl;
  E.log "\n%s\n" "Resume Lists";
  H.iter iterate_h init_rtabl;
*)
(*   E.log "Blocking tasks:\n\n\n";
  E.log "%s\n" (S.concat " " !bloking); *)


  E.log "SHARED ACCESSES:\n\n\n";
  L.iter iter_out !outlist;
  E.log "\n\nCONFLICTS:\n\n\n";
    
    
      for i = 0 to ((L.length !outlist) - 1) do
        ( ( let (c1,l1,s1,s2,s3,p1,s4) = (L.nth !outlist i) in
            for j = (i + 1) to ((L.length !outlist) - 1) do
            ( ( let (c2,l2,s5,s6,s7,p2,s8) = (L.nth !outlist j) in
                if( (s1 <> s5) && ((s3 = "W") || (s7 = "W") ) && (s2 = s6) (*&& (L.exists (dupl_h s2) !globvars) && (L.exists (dupl_h s6) !globvars)*) )
                then 
                ( b_int_a := true;
                  a_int_b := true;
                  E.log "%d conflicts %d\n" c1 c2;
                  (*E.log "[%d %a;%s] conflicts [%d %a;%s]\n" c1 d_loc l1 s3 c2 d_loc l2 s7;*)
                (*E.log "\nChecking b_int_a: %s\n" (string_of_bool !b_int_a);*)
                  for x = 0 to ((L.length s4) - 1) do
                  ( match (L.nth s4 x) with 
                    | "C" -> b_int_a := false
                    | "I" -> b_int_a := false
                    | "A" -> (check_A s8 0)
                    | "P" -> 
                    ( (check_P p1 p2 s1 0);
                    (check_N p1 s5 0 s8 s1) )
                    | st1 when (st1 = s5) -> (check_B p1 s5 0 s8 s1)
                    | mutex1 when (L.exists (dupl_m mutex1) !mutexlist) -> (check_m s8 mutex1 0)
                    | flag1 when ((dupl_h "set_" (S.sub flag1 0 4)) && (L.exists (dupl_h (S.sub flag1 4 ((S.length flag1)- 4))) !flag_list)) -> 
                    ( check_set_flag p1 (S.sub flag1 4 ((S.length flag1)- 4)) 0 s8 s1 )
                    (* | flag2 when ((dupl_h "check_" (S.sub flag2 0 6)) && (L.exists (dupl_h (S.sub flag2 6 ((S.length flag2)- 6))) !flag_list)) -> (E.log "Checking for flag check condition for %s\n" flag2) *)
                    | _ -> () )
                done;
                (*E.log "%s %s\n" "Inside b_int_a" (string_of_bool !b_int_a);*)
      
                (*E.log "\nChecking a_int_b: %s\n" (string_of_bool !a_int_b);*)
                for x = 0 to ((L.length s8) - 1) do
                ( match (L.nth s8 x) with
                  | "C" -> a_int_b := false
                  | "I" -> a_int_b := false
                  | "A"-> (check_A s4 1)
                  | "P" -> 
                    ( (check_P p2 p1 s5 1);
                    (check_N p2 s1 1 s4 s5) )
                  | st1 when (st1 = s1) -> (check_B p2 s1 1 s4 s5)
                  | mutex1 when (L.exists (dupl_m mutex1) !mutexlist) -> (check_m s4 mutex1 1)
                  | flag1 when ((dupl_h "set_" (S.sub flag1 0 4)) && (L.exists (dupl_h (S.sub flag1 4 ((S.length flag1)- 4))) !flag_list)) -> 
                    ( check_set_flag p2 (S.sub flag1 4 ((S.length flag1)- 4)) 1 s4 s5 )
                  | _ -> () )
              done;
              (*E.log "%s %s\n" "Inside a_int_b" (string_of_bool !a_int_b);*)
      
              if(!b_int_a || !a_int_b) then 
              ( pdrcnt := ( !pdrcnt + 1);
                E.log "%s\n\n" "POTENTIAL DATA-RACE") 
            else ( drfcnt := ( !drfcnt + 1); 
              E.log "%s\n\n" "DATA-RACE FREE") ) ) )
          done ) )
      done;
  
  E.log "\n\nTotal Conflicts: %d\nPotential Data-Races: %d\nRace-Free Pairs: %d\n" (!pdrcnt + !drfcnt) !pdrcnt !drfcnt;
  E.log "\nTotal Time: %f\n" (Sys.time() -. !s_tim)




let rec feature = 
{ fd_name = "may_race";              
  fd_enabled = false;
  fd_description = "may_race analysis";
  fd_extraopt = [
  "--im_ceil", Arg.String (fun s1 -> feature.fd_enabled <- true;
                                     im_ceil := s1 ), 
                "=y Enable immediate priority ceiling. default is priority inheritance";
  "--eq_fcfs", Arg.String (fun s2 -> feature.fd_enabled <- true;
                                     eq_fcfs := s2 ), 
                "=y Enable equal priority FCFS. default is round robin";
  "--flag", Arg.String (fun s3 -> feature.fd_enabled <- true;
                                     flag_list := s3::!flag_list ), 
                "=flag_name Check flag conditions for \"flag_name\"";
  ];
  fd_doit = mainfn;
  fd_post_check = true
} 

let () = Feature.register feature
